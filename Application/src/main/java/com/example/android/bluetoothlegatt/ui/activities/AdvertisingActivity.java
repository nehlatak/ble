package com.example.android.bluetoothlegatt.ui.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.model.AdvertData;
import com.example.android.bluetoothlegatt.services.BluetoothLeService;
import com.example.android.bluetoothlegatt.ui.view.ChartView;
import com.example.android.bluetoothlegatt.R;
import com.example.android.bluetoothlegatt.model.RulerResult;
import com.example.android.bluetoothlegatt.ui.base.BaseActivity;
import com.squareup.otto.Subscribe;

import butterknife.Bind;
import butterknife.OnClick;

public class AdvertisingActivity extends BaseActivity {
    /*@Bind(R.id.x)
    TextView xValue;

    @Bind(R.id.y)
    TextView yValue;

    @Bind(R.id.z)
    TextView zValue;

    @Bind(R.id.time)
    TextView time;

    @Bind(R.id.battery)
    TextView battery;

    @Bind(R.id.state)
    TextView state;

    @Bind(R.id.graph1)
    ChartView graph1;

    @Bind(R.id.graph2)
    ChartView graph2;*/

    @Bind(R.id.rows)
    TextView rowsCounter;

    private BluetoothLeService mBluetoothLeService;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            mBluetoothLeService.startAdvertisingScan();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService.stopAdvertisingScan();
            mBluetoothLeService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertising250716);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent bleServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(bleServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mServiceConnection);
    }

    @Subscribe
    public void progress(final AdvertData data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (rowsCounter != null) {
                    rowsCounter.setText(String.valueOf(data.counter));
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBluetoothLeService.resetWriteParams();
    }

    /*@Subscribe
    public void progress(final RulerResult result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                xValue.setText(String.valueOf(result.x));
                yValue.setText(String.valueOf(result.y));
                zValue.setText(String.valueOf(result.z));
                java.util.Date deviceTime = new java.util.Date((long) result.time * 1000);
                time.setText(String.valueOf(deviceTime));
                battery.setText(String.valueOf(result.battery));
                state.setText(String.valueOf(result.state));

                graph1.drawPoint(result.x, result.y);
                graph2.drawPoint(0, result.z);


            }
        });
    }

    @OnClick(R.id.stopAdv)
    public void submit(View view) {
        if (mBluetoothLeService != null) {
            mBluetoothLeService.stopAdvertisingScan();
        }
    }

    @OnClick(R.id.clear)
    public void clear(View view) {
        graph1.clear();
        graph2.clear();
    }*/
}