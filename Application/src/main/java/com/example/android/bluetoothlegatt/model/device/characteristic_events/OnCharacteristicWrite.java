package com.example.android.bluetoothlegatt.model.device.characteristic_events;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

public class OnCharacteristicWrite {
    public BluetoothGatt gatt;
    public BluetoothGattCharacteristic characteristic;
    public int status;

    public OnCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        this.gatt = gatt;
        this.characteristic = characteristic;
        this.status = status;
    }

    public BluetoothGattCharacteristic getCharacteristic() {
        return characteristic;
    }

    public int getStatus() {
        return status;
    }

    public BluetoothGatt getGatt() {
        return gatt;
    }
}