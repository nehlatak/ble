package com.example.android.bluetoothlegatt;

import android.util.Log;

import com.example.android.bluetoothlegatt.services.BluetoothLeService;

public class WatchThread extends Thread {
    private BluetoothLeService service;
    private boolean loopFlag = true;

    public WatchThread(BluetoothLeService service) {
        this.service = service;
    }

    @Override
    public void run() {
        super.run();
        long chunkTime = 0;
        int connectionState = 0;
        while (loopFlag) {
            chunkTime = service.chunkTime;
            connectionState = service.mConnectionState;
            if (connectionState == BluetoothLeService.STATE_CONNECTED) {
                long curTime = System.currentTimeMillis();
                if (service.isSendStarted) {
                    if ((curTime - chunkTime) > 2000) {
                        Log.d("WATCH", "chunk time difference = " + String.valueOf(curTime - chunkTime));
                        service.disconnect();
                    }
                }
            } else {
                Thread.yield();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.d("DEBUG", e.toString());
            }
        }
    }

    public void stopThread() {
        synchronized (service.bleSync) {
            loopFlag = false;
        }
    }
}
