package com.example.android.bluetoothlegatt.ui.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;

import com.example.android.bluetoothlegatt.R;

/**
 * Main activity, with button to toggle phone calls detection on and off.
 */
public class Advertising extends Activity {

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager bluetoothManager;
    private android.bluetooth.le.BluetoothLeAdvertiser mLeAdvertiser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertising_test);

        enableBluetooth(true);

        bluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothManager == null) Log.e("onCreate", "bluetoothManager service is NULL!!!!");
        if (mBluetoothAdapter == null) Log.e("onCreate", "mBluetoothAdapter is NULL!!!!!");

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.d("onCreate", "BLE feature is NOT available");
        } else {
            Log.d("onCreate", "BLE feature is available");
            startAdvertising();
        }
    }

    public static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public static void enableBluetooth(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Log.e("enableBluetooth", "Not switching bluetooth since its not present");
            return;
        }
        if (bluetoothAdapter.isEnabled() == enable) {
            Log.e("enableBluetooth", "BT is enabled");
            if (bluetoothAdapter.isMultipleAdvertisementSupported()) {
                Log.d("Capability", "Multiple Advertisements supported");
            } else {
                Log.d("Capability", "Multiple Advertisements NOT supported!");
            }
            return;
        }
        if (enable) {
            bluetoothAdapter.enable();
        } else {
            bluetoothAdapter.disable();
        }
        Log.i("enableBluetooth", "Switched bluetooth to " + enable);
    }

    private void startAdvertising() {
        ParcelUuid mAdvParcelUUID = ParcelUuid.fromString("0000FEFF-0000-1000-8000-00805F9B34FB");

        mLeAdvertiser = (BluetoothLeAdvertiser) ((BluetoothAdapter) ((BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter()).getBluetoothLeAdvertiser();
        if (mLeAdvertiser == null) {
            Log.e("startAdvertising", "didn't get a bluetooth le advertiser");
            return;
        }

        AdvertiseSettings.Builder mLeAdvSettingsBuilder =
                new AdvertiseSettings.Builder().setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);
        mLeAdvSettingsBuilder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER);
        mLeAdvSettingsBuilder.setConnectable(false);
        AdvertiseData.Builder mLeAdvDataBuilder = new AdvertiseData.Builder();

        List<ParcelUuid> myUUIDs = new ArrayList<ParcelUuid>();
        myUUIDs.add(ParcelUuid.fromString("0000FE00-0000-1000-8000-00805F9B34FB"));
        byte mServiceData[] = {(byte) 0xff, (byte) 0xfe, (byte) 0x00, (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04};
        mLeAdvDataBuilder.addServiceData(mAdvParcelUUID, mServiceData);

        AdvertiseSettings.Builder advSetBuilder = new AdvertiseSettings.Builder();
        advSetBuilder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY);
        advSetBuilder.setConnectable(false);
        advSetBuilder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM);
        advSetBuilder.setTimeout(10000);
        Log.d("advBuild", "settings:" + advSetBuilder.build());

        AdvertiseData.Builder advDataBuilder = new AdvertiseData.Builder();
        advDataBuilder.setIncludeDeviceName(false);
        advDataBuilder.setIncludeTxPowerLevel(true);
        advDataBuilder.addServiceData(mAdvParcelUUID, mServiceData);
        mLeAdvertiser.startAdvertising(mLeAdvSettingsBuilder.build(), mLeAdvDataBuilder.build(), mLeAdvCallback);
    }

    /**
     * Stop Advertisements
     */
    public void stopAdvertisements() {
        if (mLeAdvertiser != null) {
            mLeAdvertiser.stopAdvertising(mLeAdvCallback);
        }
    }

    private final AdvertiseCallback mLeAdvCallback = new AdvertiseCallback() {
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            Log.d("AdvertiseCallback", "onStartSuccess:" + settingsInEffect);
        }

        public void onStartFailure(int errorCode) {
            String description = "";
            if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED)
                description = "ADVERTISE_FAILED_FEATURE_UNSUPPORTED";
            else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS)
                description = "ADVERTISE_FAILED_TOO_MANY_ADVERTISERS";
            else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED)
                description = "ADVERTISE_FAILED_ALREADY_STARTED";
            else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE)
                description = "ADVERTISE_FAILED_DATA_TOO_LARGE";
            else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR)
                description = "ADVERTISE_FAILED_INTERNAL_ERROR";
            else description = "unknown";
            Log.e("AdvertiseCB", "onFailure error:" + errorCode + " " + description);
        }
    };

}