package com.example.android.bluetoothlegatt.model.result_handlers;

import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.SensorsData;

import java.nio.ByteBuffer;

/**
 * Created by dracula on 31.10.2016.
 */

public class SimpleHandler1 implements ResultDataHandler {
    @Override
    public void handleResult(byte[] scanRecord) {
        short sensor1 = ByteBuffer.wrap(new byte[]{scanRecord[0], scanRecord[1]}).getShort();
        short sensor2 = ByteBuffer.wrap(new byte[]{scanRecord[2], scanRecord[3]}).getShort();
        short sensor3 = ByteBuffer.wrap(new byte[]{scanRecord[4], scanRecord[5]}).getShort();

        SensorsData sensorsData = new SensorsData(sensor1, sensor2, sensor3);
        App.bus.post(sensorsData);

//        String result = "sensor1 = " + String.valueOf(sensor1) + " sensor2 = " + String.valueOf(sensor2) + " sensor3 = " + String.valueOf(sensor3);
    }
}
