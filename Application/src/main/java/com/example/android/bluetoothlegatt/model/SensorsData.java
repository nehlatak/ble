package com.example.android.bluetoothlegatt.model;

/**
 * Created by dracula on 31.10.2016.
 */

public class SensorsData {
    short sensor1;
    short sensor2;
    short sensor3;

    public SensorsData(short sensor1, short sensor2, short sensor3) {
        this.sensor1 = sensor1;
        this.sensor2 = sensor2;
        this.sensor3 = sensor3;
    }

    public short getSensor1() {
        return sensor1;
    }

    public short getSensor2() {
        return sensor2;
    }

    public short getSensor3() {
        return sensor3;
    }
}