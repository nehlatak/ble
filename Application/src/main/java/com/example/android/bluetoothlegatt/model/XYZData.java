package com.example.android.bluetoothlegatt.model;

import android.bluetooth.BluetoothDevice;

import com.example.android.bluetoothlegatt.App;

import java.nio.ByteBuffer;

/**
 * Created by dracula on 28.07.2016.
 */
public class XYZData implements DeviceDataParser {
    @Override
    public void handleData(BluetoothDevice device, int rssi, byte[] scanRecord) {
        String mac = device.getAddress();
        int i = 0;
        while (i < scanRecord.length) {
            int length = scanRecord[i];
            int type = scanRecord[i] + 1;
            int typeCount = i + 1;

            if (type == 18) {
                byte number1array[] = new byte[2];
                number1array[1] = scanRecord[typeCount + 1];
                number1array[0] = scanRecord[typeCount + 2];

                byte number2array[] = new byte[2];
                number2array[1] = scanRecord[typeCount + 3];
                number2array[0] = scanRecord[typeCount + 4];

                byte number3array[] = new byte[2];
                number3array[1] = scanRecord[typeCount + 5];
                number3array[0] = scanRecord[typeCount + 6];

                byte time[] = new byte[4];
                time[3] = scanRecord[typeCount + 7];
                time[2] = scanRecord[typeCount + 8];
                time[1] = scanRecord[typeCount + 9];
                time[0] = scanRecord[typeCount + 10];

                byte battery[] = new byte[2];
                battery[1] = scanRecord[typeCount + 11];
                battery[0] = scanRecord[typeCount + 12];

                ByteBuffer xValue = ByteBuffer.wrap(number1array); // big-endian by default
                ByteBuffer yValue = ByteBuffer.wrap(number2array); // big-endian by default
                ByteBuffer zValue = ByteBuffer.wrap(number3array); // big-endian by default
                ByteBuffer wrapped3 = ByteBuffer.wrap(time);
                ByteBuffer wrapped4 = ByteBuffer.wrap(battery);

                RulerResult result = new RulerResult();
                result.x = xValue.getShort();
                result.y = yValue.getShort();
                result.z = zValue.getShort();
                result.time = wrapped3.getInt();
                result.battery = wrapped4.getShort();
                result.state = scanRecord[typeCount + 13];

                App.bus.post(result);
            }
            i += (length + 1);
        }
    }
}
