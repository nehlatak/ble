package com.example.android.bluetoothlegatt.model.device;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicChanged;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicRead;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicWrite;
import com.example.android.bluetoothlegatt.model.result_handlers.SimpleHandler1;
import com.example.android.bluetoothlegatt.util.AppEvents;
import com.example.android.bluetoothlegatt.util.Utils;
import com.squareup.otto.Subscribe;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

import timber.log.Timber;

public class Device {
    private SimpleHandler1 simpleHandler1 = new SimpleHandler1();

    public Device(BleConnector connector) {
        if (connector == null) {
            throw new NullPointerException("connector should not be null!");
        }
        this.connector = connector;
        this.connector.bus.register(this);
    }

    private BleConnector connector;
    public BluetoothGattService fileService = null;
    public BluetoothGattCharacteristic characteristic1 = null;
    public BluetoothGattCharacteristic characteristic2 = null;
    public BluetoothGattCharacteristic characteristic3 = null;
    public BluetoothGattCharacteristic characteristic4 = null;
    public BluetoothGattCharacteristic characteristic6 = null;
    public BluetoothGattCharacteristic characteristic7 = null;
    public BluetoothGattCharacteristic characteristic8 = null;

    public BluetoothGattService commandService = null;
    public BluetoothGattCharacteristic characteristic61 = null;
    public BluetoothGattCharacteristic characteristic62 = null;
    public BluetoothGattCharacteristic characteristic63 = null;
    public BluetoothGattCharacteristic characteristic64 = null;

    private void setCharacteristics() {
        Log.d("Device", "DeviceCharacteristics.fileServiceUUID = " + String.valueOf(DeviceCharacteristics.fileServiceUUID));
        fileService = connector.getGattService(DeviceCharacteristics.fileServiceUUID);
        characteristic1 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic1UUID));
        characteristic2 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic2UUID));
        characteristic3 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic3UUID));
        characteristic4 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic4UUID));
        characteristic6 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic6UUID));
        characteristic7 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic7UUID));
        characteristic8 = fileService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristic8UUID));

        commandService = connector.getGattService(DeviceCharacteristics.commandServiceUUID);
        characteristic61 = commandService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristicFE61));
        characteristic62 = commandService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristicFE62));
        characteristic63 = commandService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristicFE63));
        characteristic64 = commandService.getCharacteristic(UUID.fromString(DeviceCharacteristics.characteristicFE64));
        Log.d("Device", String.valueOf("set of characteristic is done!"));
    }

    @Subscribe
    public void onCharacteristicWrite(OnCharacteristicWrite event) {
//        Log.d("DEBUG", String.valueOf("OnCharacteristicWrite! from Device"));
    }

    @Subscribe
    public void onCharacteristicChanged(OnCharacteristicChanged event) {
//        Log.d("DEBUG", String.valueOf("onCharacteristicChanged!!! from Device"));
    }

    @Subscribe
    public void onCharacteristicRead(OnCharacteristicRead event) {
        byte[] data = event.getCharacteristic().getValue();

        Timber.d(Utils.byteArrayToHex(data));

        simpleHandler1.handleResult(data);
    }

    @Subscribe
    public void onServicesDiscovered(AppEvents event) {
        switch (event) {
            case DEVICE_SERVICES_DISCOVERED:
                setCharacteristics();
                break;
        }
    }

    public void prepare() {
        byte[] bytes = {0x03, 0x01};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    public void prepare2() {
        byte[] bytes = {0x03, 0x02};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    public void start() {
        byte[] bytes = {0x04, (byte) 0xff};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    public void stop() {
        byte[] bytes = {0x04, (byte) 0x00};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    public void slowStart() {
        byte[] bytes = {0x05, (byte) 0xff};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    public void slowStop() {
        byte[] bytes = {0x06, (byte) 0x00};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    //мотор постоянно работает по программе
    public void prog1() {
        byte[] bytes = {0x08, (byte) 0x00};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    //мотор постоянно работает по программе, но не будет останавливаться при падении напряжения
    public void prog2() {
        byte[] bytes = {0x09, (byte) 0x00};
        connector.writeCharacteristic(characteristic62, bytes);
    }

    public void lightOn() {
        byte[] bytes = {0x03, 0x02};
        connector.writeCharacteristic(characteristic62, bytes);

        byte[] bytes2 = {0x04, (byte) 0xff};
        connector.writeCharacteristic(characteristic62, bytes2);
    }

    public void lightOff() {
        stop();
    }

    public void writeTime() {
        int unixtime = (int) (System.currentTimeMillis() / 1000L);
        byte[] unixtimeBytes = Utils.reverseArray(ByteBuffer.allocate(4).putInt(unixtime).array());
        connector.writeCharacteristic(characteristic64, unixtimeBytes);
    }

    public void writeTime(int value) {
        byte[] unixtimeBytes = Utils.reverseArray(ByteBuffer.allocate(4).putInt(value).array());
//        byte[] unixtimeBytes = ByteBuffer.allocate(4).putInt(value).array();
        Timber.d("time = " + Utils.byteArrayToHex(unixtimeBytes));
        connector.writeCharacteristic(characteristic64, unixtimeBytes);
    }

    public void wakeUp() {
        connector.writeCharacteristic(characteristic62, new byte[]{(byte) 0x19, 0x00});
    }

    public void sleep() {
        connector.writeCharacteristic(characteristic62, new byte[]{(byte) 0x19, 0x01});
    }

    public void enableLightSensor() {
        byte[] enlight = new byte[]{(byte) 0x20, 0x01};
        Timber.d("enlight = " + Utils.byteArrayToHex(enlight));
        connector.writeCharacteristic(characteristic62, enlight);
    }

    public void readStatData() {
        (new Thread() {
            public void run() {
                int i = 0;
                while (true) {
                    try {
                        connector.readCharacteristic(characteristic61);
                        i++;
                        Thread.sleep(400); //read stat data
                    } catch (InterruptedException e) {
                        Log.d("Device", e.toString());
                    }
                }
            }
        }).start();
    }
}