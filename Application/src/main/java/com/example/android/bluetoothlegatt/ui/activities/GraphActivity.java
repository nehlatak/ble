package com.example.android.bluetoothlegatt.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.example.android.bluetoothlegatt.model.LineGraph;
import com.example.android.bluetoothlegatt.model.Point;

import org.achartengine.GraphicalView;

public class GraphActivity extends Activity {
    private GraphicalView graphView;
    private LineGraph lineGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_graph);

        lineGraph = new LineGraph();
        graphView = lineGraph.getView(this);
        setContentView(graphView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4000; i++) {
                    final int x = i;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lineGraph.addNewPoints1(Point.randomPoint(x));
                            lineGraph.addNewPoints2(Point.randomPoint(x));
                            graphView.repaint();
                        }
                    });

                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        Log.d("DEBUG", String.valueOf("InterruptedException!!!"));
                    }
                }
            }
        }).start();
    }
}