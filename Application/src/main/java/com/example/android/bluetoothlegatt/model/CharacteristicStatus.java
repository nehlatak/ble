package com.example.android.bluetoothlegatt.model;

/**
 * Created by dracula on 26.07.2016.
 */
public class CharacteristicStatus {
    private int status;

    public CharacteristicStatus setStatus(int status) {
        this.status = status;
        return this;
    }

    public int getStatus() {
        return status;
    }
}
