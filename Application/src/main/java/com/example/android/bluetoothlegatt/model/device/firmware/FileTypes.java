package com.example.android.bluetoothlegatt.model.device.firmware;

import java.util.ArrayList;

/**
 * Created by dracula on 16.08.2016.
 */
public class FileTypes {
    public static final ArrayList<String> types = new ArrayList<>();

    {
        types.add("softdevice");
        types.add("app");
    }
}