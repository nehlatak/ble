package com.example.android.bluetoothlegatt.model.device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.support.v4.util.Pair;
import android.util.Log;

import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.RulerResult;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicChanged;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicRead;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicWrite;
import com.example.android.bluetoothlegatt.util.AppEvents;
import com.example.android.bluetoothlegatt.util.Utils;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import timber.log.Timber;

public class BleConnector {
    public Bus bus;
    private Context context;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothDevice device;
    private int mConnectionState = STATE_DISCONNECTED;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private String deviceAddress;
    private int connectCount;
    private boolean disconnectFlag;
    private static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    private Queue<BluetoothGattCharacteristic> characteristicsNotificationsQueue = new LinkedList<>();
    private Queue<Pair<BluetoothGattCharacteristic, byte[]>> characteristicsToWriteQueue = new LinkedList<>();
    private boolean notificationFlag = false;

    public BleConnector(Context context) {
        this.context = context;
        bus = new Bus(ThreadEnforcer.ANY);
        connectCount = 0;
        disconnectFlag = false;
    }

    /**
     * Инициализация блютуза
     */
    public boolean initialize() {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e("DEBUG", "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e("DEBUG", "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    connectCount = 0;
                    bus.post(AppEvents.DEVICE_STATE_CONNECTED);
                    mConnectionState = STATE_CONNECTED;
                    Timber.d("Connected to GATT server!");
                    App.bus.post(AppEvents.DEVICE_STATE_CONNECTED);
                    boolean servicesDiscover = mBluetoothGatt.discoverServices();
                    Timber.d("Attempting to start service discovery:" + servicesDiscover);
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    bus.post(AppEvents.DEVICE_STATE_DISCONNECTED);
                    close();
                    mConnectionState = STATE_DISCONNECTED;
                    App.bus.post(AppEvents.DEVICE_STATE_DISCONNECTED);
                    Timber.d("Disconnected from GATT server!");
                    if (!disconnectFlag && connectCount < 10) {
                        connectToDevice(deviceAddress);
                    }
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                bus.post(AppEvents.DEVICE_SERVICES_DISCOVERED);
                App.bus.post(AppEvents.DEVICE_SERVICES_DISCOVERED);
                Timber.d(String.valueOf("build characteristic event send!"));//Можно строить характеристики
            } else {
                Timber.d("onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                bus.post(new OnCharacteristicRead(gatt, characteristic, status));
                broadcastUpdate(AppEvents.DEVICE_ON_CHARACTERISTIC_READ, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Timber.d(String.valueOf("onCharacteristicChanged!!!"));
            bus.post(new OnCharacteristicChanged(gatt, characteristic));
            broadcastUpdate(AppEvents.ACTION_DATA_AVAILABLE, characteristic);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            bus.post(new OnCharacteristicWrite(gatt, characteristic, status));
            if (!notificationFlag) {
                writeAllCharacteristics();
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Timber.d("Callback: Wrote GATT Descriptor successfully.");
            } else {
                Timber.d("Callback: Error writing GATT Descriptor: " + status);
            }
            characteristicsNotificationsQueue.poll();
            if (characteristicsNotificationsQueue.size() > 0) {
                Timber.d("characteristicsNotificationsQueue.element()");
                setNotification(characteristicsNotificationsQueue.element());
            } else {
                notificationFlag = false;
                writeAllCharacteristics();
                Timber.d("writeAllCharacteristics()");
            }
        }
    };


    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        disconnectFlag = true;
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w("DEBUG", "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
        mBluetoothGatt = null;
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w("DEBUG", "BluetoothAdapter not initialized");
            return;
        }
        if (!notificationFlag) {
            Timber.d("!notificationFlag");
            Timber.d("setNotification(characteristic)");
            notificationFlag = true;
            characteristicsNotificationsQueue.add(characteristic);
            setNotification(characteristic);
        } else {
            Timber.d("notificationFlag");
            Timber.d("characteristicsNotificationsQueue.add(characteristic)");
            characteristicsNotificationsQueue.add(characteristic);
        }
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] bytes) {
        if (notificationFlag) {
            characteristicsToWriteQueue.add(new Pair<BluetoothGattCharacteristic, byte[]>(characteristic, bytes));
            return true;
        } else {
            return writeCharacteristicReal(characteristic, bytes);
        }
    }

    private void writeAllCharacteristics() {
        Timber.d("writeAllCharacteristics function");
        if (characteristicsToWriteQueue.size() > 0) {
            writeCharacteristicReal(characteristicsToWriteQueue.element().first, characteristicsToWriteQueue.element().second);
            characteristicsToWriteQueue.poll();
        }
    }

    private boolean writeCharacteristicReal(BluetoothGattCharacteristic characteristic, byte[] bytes) {
        Timber.d("writeCharacteristicReal");
        if (mConnectionState == STATE_DISCONNECTED) {
            Timber.d(String.valueOf("cant write! device disconnected!"));
            return false;
        }
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Timber.d("writeCharacteristic() : BluetoothAdapter not initialized");
            return false;
        }
        boolean result;
        if (characteristic != null) {
            int properties = characteristic.getProperties();

            if (((properties & BluetoothGattCharacteristic.PROPERTY_WRITE) == BluetoothGattCharacteristic.PROPERTY_WRITE)
                    || ((properties & BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE) == BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE)
                    || ((properties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) == BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) {

                characteristic.setValue(bytes);
                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                result = mBluetoothGatt.writeCharacteristic(characteristic);
                Timber.d(String.valueOf("write " + characteristic.getUuid().toString() + " " + result));
            } else {
                Timber.d("CANT WRITE!");
                result = false;
            }
            return result;
        } else {
            Timber.d(String.valueOf("characteristic is null!"));
            return false;
        }
    }

    public boolean connectToDevice(String mac) {
        this.initialize();
        this.deviceAddress = mac;
        connectCount++;
        disconnectFlag = false;
        return this.connect(deviceAddress);
    }

    public BluetoothGattService getGattService(String serviceUUID) {
        return mBluetoothGatt.getService(UUID.fromString(serviceUUID));
    }

    public BluetoothGattCharacteristic getGattServiceCharacteristic(BluetoothGattService service, String characteristicUUID) {
        return service.getCharacteristic(UUID.fromString(characteristicUUID));
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w("BleConnector", "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    private boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w("DEBUG", "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w("DEBUG", "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(context, false, mGattCallback);
        Timber.d("Trying to create a new connection.");
        //App.bus.post(AppActions.TRYING_TO_CONNECT);

        mConnectionState = STATE_CONNECTING;
        return true;
    }

    private void setNotification(BluetoothGattCharacteristic characteristic) {
        Timber.d("setNotification function");
        mBluetoothGatt.setCharacteristicNotification(characteristic, true);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));  //00002902-0000-1000-8000-00805f9b34fb
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
        Timber.d("writeDescriptor");
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    private void broadcastUpdate(final String action) {
        final BleEvent event = new BleEvent(action);
        App.bus.post(event);
    }

    private void broadcastUpdate(final AppEvents action, final BluetoothGattCharacteristic characteristic) {
        final BleEvent event = new BleEvent();
        event.setEvent(action);

        // For all other profiles, writes the data formatted in HEX.
        final byte[] data = characteristic.getValue();

        if (data != null && data.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(data.length);
            for (byte byteChar : data) {
                stringBuilder.append(String.format("%02X ", byteChar));
            }

            event.setInfo(new String(data) + "\n" + stringBuilder.toString());
            App.bus.post(event);
        }
    }

    public void startAdvertisingScan() {
        if (mBluetoothAdapter != null) {
            this.disconnect();
            mBluetoothAdapter.startLeScan(ScanCallback);    //advertising listening enable
        }
    }

    public void stopAdvertisingScan() {
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(ScanCallback);    //advertising listening disable
        }
    }

    //BLE ADVERTISING
    private final BluetoothAdapter.LeScanCallback ScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            String mac = device.getAddress();
//            Log.d("DEBUG", "mac = " + mac);
            /*Log.d("DEBUG", "addres = " + device.getAddress());
            Log.d("DEBUG", "name = " + device.getName());*/

            int i = 6;

            if (mac.equals(deviceAddress)) {
                Log.d("DEBUG", "result = " + Utils.byteArrayToHex(scanRecord));

                byte number1array[] = new byte[2];
                number1array[1] = scanRecord[i + 1];
                number1array[0] = scanRecord[i + 2];

                byte number2array[] = new byte[2];
                number2array[1] = scanRecord[i + 3];
                number2array[0] = scanRecord[i + 4];

                byte number3array[] = new byte[2];
                number3array[1] = scanRecord[i + 5];
                number3array[0] = scanRecord[i + 6];

                byte battery[] = new byte[2];
                battery[1] = scanRecord[i + 9];
                battery[0] = scanRecord[i + 10];

                byte temperature[] = new byte[2];
                temperature[1] = scanRecord[i + 11];
                temperature[0] = scanRecord[i + 12];

                ByteBuffer xValue = ByteBuffer.wrap(number1array);
                ByteBuffer yValue = ByteBuffer.wrap(number2array);
                ByteBuffer zValue = ByteBuffer.wrap(number3array);
                ByteBuffer wrappedbattery = ByteBuffer.wrap(battery);
                ByteBuffer wrappedTemp = ByteBuffer.wrap(temperature);

                RulerResult result = new RulerResult();
                result.x = xValue.getShort();
                result.y = yValue.getShort();
                result.z = zValue.getShort();

                result.f = scanRecord[i + 7];
                result.m = scanRecord[i + 8];

                result.battery = wrappedbattery.getShort();

                result.rssi = rssi;
                result.deviceAddress = deviceAddress;

                double fullPartTemp = ((double) (wrappedTemp.getShort()) / 4.0);

                result.temp = fullPartTemp;

                App.bus.post(result);
            }
        }
    };
}