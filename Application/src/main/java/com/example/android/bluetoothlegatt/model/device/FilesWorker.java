package com.example.android.bluetoothlegatt.model.device;

import android.util.Log;

import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicChanged;
import com.example.android.bluetoothlegatt.model.device.characteristic_events.OnCharacteristicWrite;
import com.example.android.bluetoothlegatt.util.Utils;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import timber.log.Timber;

public class FilesWorker {
    public static final String repoUrl = "http://192.168.16.11/firmware/fork/";
    public static final String fileSecretStr = "Vo vsem vinovaty popy!";
    public static final byte[] firmwareFileId = {0x00, 0x00, 0x4e, (byte) 0x8b};
    public static final byte[] stackFileId = {0x00, 0x00, 0x4e, (byte) 0x8f};
    public static int fileRowsCounter = 99;

    public static final String repoFileName = "repo";
    public static final String firmwareFileName = "app_gyro3_0.1.bin";
    public static final String stackFileName = "s132_nrf52_2.0.0_softdevice.bin";

    public FilesWorker(BleConnector connector, Device device) {
        if (connector == null) {
            throw new NullPointerException("connector should not be null!");
        }
        if (device == null) {
            throw new NullPointerException("device should not be null!");
        }
        this.connector = connector;
        this.connector.bus.register(this);
        this.device = device;
    }

    private BleConnector connector;
    private Device device;
    private int chunkCount;//количество отправленных секций прошивки
    private int sectionCount;//счетчик отправляемых секций по 4К
    private int fileBytesToDeviceSize;
    private ArrayList<ArrayList<Byte>> chunks = new ArrayList<>();
    private byte[] fileBytesToDevice;
    private boolean writeFileFlag;
    public final Object bleSync = new Object();

    public static String getCachePath() {
        return App.getContext().getCacheDir().getPath();
    }

    public static void getCacheFilesList() {
        String[] names;
        if (App.getContext().getCacheDir().isDirectory()) {
            names = App.getContext().getCacheDir().list();
            Log.d("DEBUG", Arrays.toString(names));
        }
    }

    @Subscribe
    public void onCharacteristicWrite(OnCharacteristicWrite event) {
        String charactUuid = event.characteristic.getUuid().toString();
        if (charactUuid.equals(DeviceCharacteristics.characteristic3UUID)) {
            Log.d("FilesWorker", String.valueOf("chunkCount = " + chunkCount + " chunks.size() = " + chunks.size()));
            if (chunkCount < chunks.size()) {
                if (sectionCount != 256) {
                    sendChunk(chunks.get(chunkCount));  //включить при записи файла
                } else {
                    sectionCount = 0;
                }
            }
        }
    }

    @Subscribe
    public void onCharacteristicChanged(OnCharacteristicChanged event) {
        String charactUuid = event.characteristic.getUuid().toString();
        Log.d("FilesWorker", "onCharacteristicChanged!!!");

        if (charactUuid.equals(DeviceCharacteristics.characteristic2UUID)) {
            byte[] characteristicValue = event.characteristic.getValue();

            Log.d("FilesWorker", "notif2 = " + Utils.byteArrayToHex(characteristicValue));
            Log.d("FilesWorker", String.valueOf(characteristicValue[0]));

            switch (characteristicValue[0]) {
                case 1:
                    Log.d("FilesWorker", String.valueOf("start send!"));
                    sendFileToDevice(fileBytesToDevice);
                    /*wt = new WatchThread(thisService);
                    wt.start();*/
                    break;
                case 33://FT_EndStreamingOp = 0x21
                    //продолжаем слать после записи 4К
                    if (chunkCount < chunks.size()) {
                        sendChunk(chunks.get(chunkCount));
                    }

                    Timber.d("chunkCount = " + String.valueOf(chunkCount));
                    Timber.d("chunks.size() = " + String.valueOf(chunks.size()));
                    break;
                case 86://FT_TX_Complete = 0x56
//                    buildFileFromDevice();
                    break;
                case 80://FT_Result_IntFlashOK = 0x50
//                    stopSend();
//                    broadcastUpdate(DONE);
//                    Toast.makeText(App.getContext(), "Firmware was sended!", Toast.LENGTH_SHORT).show();
                    Log.d("FilesWorker", String.valueOf("File was sent!"));
                    break;
                case 19://FT_SectMissing = 0x13
//                    stopSend();
//                    broadcastUpdate(ERROR_13);
                    break;
                case 20://FT_SectUnacceptable = 0x14
//                    stopSend();
//                    broadcastUpdate(ERROR_14);
                    break;
                case 96://FT_Result_ErrLen = 0x60
//                    stopSend();
//                    broadcastUpdate(ERROR_60);
                    break;
                case 97://FT_Result_ErrCRC = 0x61
//                    stopSend();
//                    broadcastUpdate(ERROR_61);
                    break;
                case 98://FT_UnknID = 0x62
//                    stopSend();
//                    broadcastUpdate(ERROR_62);
                    break;
                case 99:// FT_IntFlashError = 0x63
//                    stopSend();
//                    broadcastUpdate(ERROR_63);
                    break;
                case 100://FT_IntFlashCRCError = 0x64
//                    stopSend();
//                    broadcastUpdate(ERROR_64);
                    break;
                case 101://FT_InvalidAddress = 0x65
//                    stopSend();
//                    broadcastUpdate(ERROR_65);
                    break;
                case 102://FT_Result_NoFileAvailable = 0x66
                    break;
            }
        }
    }

    private void sendChunk(ArrayList<Byte> bytes) {
        synchronized (bleSync) {
            byte[] result = Utils.toPrimitives(bytes);

            boolean chunkWrite;
            chunkWrite = connector.writeCharacteristic(device.characteristic3, result);

            if (!chunkWrite) {
                Log.d("FilesWorker", "cant write to characteristic3 in sendchunk Function");
            }

            chunkCount++;
            sectionCount++;
            int[] progressUpdate = {fileBytesToDeviceSize / 16, chunkCount};
            App.bus.post(progressUpdate);
        }
    }

    public void uploadFirmwareToDevice(byte[] fileId, String fileName) {
        if (isFirmwareFileNormal()) {
            Log.d("FilesWorker", String.valueOf("upload firmware to device is ready!"));
            fileWriteInit(fileId, fileName);
        } else {
            Log.d("FilesWorker", String.valueOf("wrong firmware file!"));
        }
    }

    public void uploadStackToDevice(byte[] fileId, String fileName) {
        getCacheFilesList();
        if (isStackFileNormal()) {
            Log.d("FilesWorker", String.valueOf("upload stack to device is ready!"));
            fileWriteInit(fileId, fileName);
        } else {
            Log.d("FilesWorker", String.valueOf("wrong firmware file!"));
        }
    }

    public static boolean isFirmwareFileNormal() {
        return isFileNormal(firmwareFileName);
    }

    public static boolean isStackFileNormal() {
        return isFileNormal(stackFileName);
    }

    public static boolean isFileNormal(final String fileName) {
        if (fileName != null && !fileName.equals("")) {
            try {
                String repoStr = Utils.getStringFromFile(getCachePath() + "/" + repoFileName);
                String[] files = repoStr.split("\n");//находим строки с инфой о файлах
                for (String file : files) {
                    String[] repoParams = file.split(" ");//собираем из строки инфу о файле
                    String fileNameValue = repoParams[2];
                    if (fileNameValue.equals(fileName)) {
                        Log.d("FilesWorker", "match the names!");
                        long repoCrc32 = Long.parseLong(repoParams[0]);
                        long repoFileSize = Long.parseLong(repoParams[1]);

                        File firmware = new File(getCachePath() + "/" + fileName);
                        long fileSize = firmware.length();

                        if (repoFileSize == fileSize) {
                            Log.d("FilesWorker", "fileSize is OK!");
                            byte[] firmwareData = new byte[(int) firmware.length()];
                            try {
                                new FileInputStream(firmware).read(firmwareData);
                            } catch (Exception e) {
                                Log.d("FilesWorker", e.toString());
                            }

                            String secretStr = fileSecretStr;
                            char[] symbols = secretStr.toCharArray();
                            boolean firmwareFlag = true;

                            if (!fileName.equals(stackFileName)) {//грязный хак
                                for (int i = 0; i < firmwareData.length; i++) {
                                    for (int j = 0; j < symbols.length; j++) {
                                        if (i + j < firmwareData.length && firmwareData[i + j] != symbols[j]) {
                                            firmwareFlag = false;
                                            continue;
                                        }
                                    }

                                    if (firmwareFlag) {
                                        Log.d("FilesWorker", "secret string was found!");
                                        long calculatedCrc32 = Utils.calcCrc32(firmwareData, firmwareData.length);
                                        calculatedCrc32 = Utils.crc32End(calculatedCrc32, firmwareData.length);

                                        if (repoCrc32 == calculatedCrc32) {
                                            Log.d("FilesWorker", "Crc32 is OK!");
                                            return true;
                                        }
                                    }
                                    firmwareFlag = true;
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.d("FilesWorker", e.toString());
                return false;
            }
        } else {
            throw new NullPointerException();
        }
        return false;
    }

    public void resetCounts() {
        chunkCount = 0;
        sectionCount = 0;
    }

    private void fileWriteInit(byte[] id, String fileName) {
        resetCounts();
        App.bus.post(new int[]{0, 0});
        writeFileFlag = true;
        long crc32;

        try {
            //извлекаем файл прошивки из интернал кеша
            File firmware = new File(getCachePath() + "/" + fileName);
            fileBytesToDevice = new byte[(int) firmware.length()];
            new FileInputStream(firmware).read(fileBytesToDevice);
            fileBytesToDeviceSize = (int) firmware.length();

            /*InputStream is = App.getContext().getAssets().open("fromDevice_33kb.txt");
            byte[] fileBytes = new byte[is.available()];
            is.read(fileBytes);
            is.close();
            fileBytesToDevice = fileBytes;
            fileBytesToDeviceSize = fileBytesToDevice.length;*/

            crc32 = Utils.calcCrc32(fileBytesToDevice, fileBytesToDeviceSize);
            crc32 = Utils.crc32End(crc32, fileBytesToDeviceSize);

            byte[] bytesId = Utils.reverseArray(ByteBuffer.allocate(4).put(id).array());
            byte[] bytesLength = Utils.reverseArray(ByteBuffer.allocate(4).putInt(fileBytesToDeviceSize).array());
            byte[] bytesCrc = Utils.reverseArray(ByteBuffer.allocate(4).putInt((int) crc32).array());
            byte[] result = ByteBuffer.allocate(12).put(bytesId).put(bytesLength).put(bytesCrc).array();

            Log.d("FilesWorker", "info str = " + Utils.byteArrayToHex(result));

            //записываем в характеристику параметры пересылаемого файла
//            service.setCharacteristicNotification(characteristic2);
            Log.d("FilesWorker", "prepare enable notification!");
            connector.setCharacteristicNotification(device.characteristic2);
            connector.writeCharacteristic(device.characteristic1, result);
        } catch (Exception e) {
            Log.d("FilesWorker", e.toString());
        }
    }

    //разбиваем файл прошивки по 18 байт и отсылаем первый кусок
    private void sendFileToDevice(byte[] bytes) {
        int chunkSize = 18;
        int sendCount = 0;
        chunks.clear();

        short k = 0;
        if (bytes.length > 0) {
            int i = 2;
            ArrayList<Byte> chunk = new ArrayList<>();

            for (int j = 0; j < bytes.length; j++) {
                chunk.add(bytes[j]);
                i++;
                if (i == chunkSize || j == (bytes.length - 1)) {
                    byte[] count = Utils.reverseArray(ByteBuffer.allocate(2).putShort(k).array());
                    chunk.add(0, count[0]);
                    chunk.add(1, count[1]);

                    chunks.add(chunk);
                    i = 2;
                    k++;
                    chunk = new ArrayList<>();
                }
            }
        }

        sendChunk(chunks.get(0));

        int[] progressUpdate = {fileBytesToDeviceSize / 16, 1};
        App.bus.post(progressUpdate);
    }

    private int calcProgress(int total, int value) {
        return (int) (value * 100) / total;
    }

    public void stopSend() {
        synchronized (bleSync) {
            writeFileFlag = false;
            resetCounts();
            /*totalChunkCount = 0;
            if (wt != null) {
                wt.stopThread();
                wt = null;
            }*/
        }
    }
}