package com.example.android.bluetoothlegatt.model.device;

import com.example.android.bluetoothlegatt.util.AppEvents;

/**
 * Created by dracula on 16.08.2016.
 */
public class BleEvent {
    private String info;
    private AppEvents event;

    public BleEvent() {
    }

    public BleEvent(String info) {
        this.info = info;
    }

    public BleEvent(String info, AppEvents event) {
        this.info = info;
        this.event = event;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setEvent(AppEvents event) {
        this.event = event;
    }

    public AppEvents getEvent() {
        return event;
    }
}