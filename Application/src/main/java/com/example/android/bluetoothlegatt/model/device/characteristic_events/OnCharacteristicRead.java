package com.example.android.bluetoothlegatt.model.device.characteristic_events;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Created by dracula on 31.10.2016.
 */

public class OnCharacteristicRead {
    private BluetoothGatt gatt;
    private BluetoothGattCharacteristic characteristic;
    private int status;

    public OnCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        this.gatt = gatt;
        this.characteristic = characteristic;
        this.status = status;
    }

    public BluetoothGattCharacteristic getCharacteristic() {
        return characteristic;
    }

    public int getStatus() {
        return status;
    }

    public BluetoothGatt getGatt() {
        return gatt;
    }
}
