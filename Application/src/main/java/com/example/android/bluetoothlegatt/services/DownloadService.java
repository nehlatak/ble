package com.example.android.bluetoothlegatt.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.device.FilesWorker;
import com.example.android.bluetoothlegatt.util.AppEvents;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadService extends IntentService {
    public static final int UPDATE_PROGRESS = 8344;

    public DownloadService() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String urlToDownload = intent.getStringExtra("url");
        String fileName = urlToDownload.substring(urlToDownload.lastIndexOf('/') + 1);
        ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
        try {
            URL url = new URL(urlToDownload);
            URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
//            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(connection.getInputStream());
            OutputStream output = new FileOutputStream(FilesWorker.getCachePath() + "/" + fileName);

            byte data[] = new byte[1024];
//            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
//                total += count;
                output.write(data, 0, count);

                // publishing the progress....
                /*Bundle resultData = new Bundle();
                resultData.putInt("progress" ,(int) (total * 100 / fileLength));
                receiver.send(UPDATE_PROGRESS, resultData);*/
            }
            output.flush();
            output.close();
            input.close();

            Bundle resultData = new Bundle();
            resultData.putInt("progress", 100);
            resultData.putString("message", urlToDownload + " was downloaded!");
            receiver.send(UPDATE_PROGRESS, resultData);

            Log.d("DownloadService", String.valueOf(urlToDownload + " was downloaded!"));
        } catch (IOException e) {
            Log.d("DEBUG", e.toString());
        }

        /*Bundle resultData = new Bundle();
        resultData.putInt("progress" ,100);
        receiver.send(UPDATE_PROGRESS, resultData);*/
    }
}