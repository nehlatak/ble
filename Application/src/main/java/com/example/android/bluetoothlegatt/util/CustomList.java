package com.example.android.bluetoothlegatt.util;

import java.util.ArrayList;

public class CustomList<E> extends ArrayList<E> {
    private int lastChangedItem;
    private int queueSize;

    @Override
    public boolean add(E object) {
        if (queueSize != 0) {
            if (this.size() == queueSize) {
                super.set(lastChangedItem, object);
                lastChangedItem++;
                if (lastChangedItem == queueSize) {
                    lastChangedItem = 0;
                }
            } else {
                super.add(object);
            }
            return true;
        } else {
            return false;
        }
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }
}