package com.example.android.bluetoothlegatt.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.android.bluetoothlegatt.Actions;
import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.R;
import com.example.android.bluetoothlegatt.model.CharacteristicStatus;
import com.example.android.bluetoothlegatt.model.DeviceDataParser;
import com.example.android.bluetoothlegatt.model.EngineData;
import com.example.android.bluetoothlegatt.model.LightSensorData;
import com.example.android.bluetoothlegatt.model.device.FilesWorker;
import com.example.android.bluetoothlegatt.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/*
* чтобы записать файл на девайс:
* - раскоментить метод fileWriteInit()
* - раскоментить вызов sendChunk() внутри onCharacteristicWrite()
* важно!!! на samsung нужно сначало писать в характеристику а потом включать нотификацию а на dexp наоборот!
* */

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
    private static final String fileServiceUUID = buildFullUuid("fef0");
    private static final String commandServiceUUID = buildFullUuid("fe60");
    private static final String characteristic1UUID = buildFullUuid("fef1");
    private static final String characteristic2UUID = buildFullUuid("fef2");
    private static final String characteristic3UUID = buildFullUuid("fef3");
    private static final String characteristic4UUID = buildFullUuid("fef4");
    private static final String characteristic5UUID = buildFullUuid("fef5");
    private static final String characteristic6UUID = buildFullUuid("fef6");
    private static final String characteristic7UUID = buildFullUuid("fef7");
    private static final String characteristic8UUID = buildFullUuid("fef8");
    private static final String characteristic9UUID = buildFullUuid("fef9");
    private static final String characteristicaUUID = buildFullUuid("fefa");
    private static final String characteristicbUUID = buildFullUuid("fefb");
    private static final String characteristiccUUID = buildFullUuid("fefc");
    /*private static final String rulerCharacteristicc61 = "f000fe61-0451-4000-b000-000000000000";
    private static final String rulerCharacteristicc62 = "f000fe62-0451-4000-b000-000000000000";
    private static final String rulerCharacteristicc63 = "f000fe63-0451-4000-b000-000000000000";
    private static final String rulerCharacteristicc64 = "f000fe64-0451-4000-b000-000000000000";*/
    private static final String rulerCharacteristicc61 = buildFullUuid("fe61");
    private static final String rulerCharacteristicc62 = buildFullUuid("fe62");
    private static final String rulerCharacteristicc63 = buildFullUuid("fe63");
    private static final String rulerCharacteristicc64 = buildFullUuid("fe64");

    private static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    private static String buildFullUuid(String shortUuid) {
//        return "f000" + shortUuid + "-0451-4000-b000-000000000000";
        return "6e40" + shortUuid + "-b5a3-f393-e0a9-e50e24dcca9e";   //nordic
    }

    public final Object bleSync = new Object();

    private BluetoothGattService service = null;
    private BluetoothGattCharacteristic characteristic1 = null;
    private BluetoothGattCharacteristic characteristic2 = null;
    private BluetoothGattCharacteristic characteristic3 = null;
    private BluetoothGattCharacteristic characteristic4 = null;
    private BluetoothGattCharacteristic characteristic6 = null;
    private BluetoothGattCharacteristic characteristic7 = null;
    private BluetoothGattCharacteristic characteristic8 = null;

    private BluetoothGattService rulerService = null;
    private BluetoothGattCharacteristic rulerCharacteristic61 = null;
    private BluetoothGattCharacteristic rulerCharacteristic62 = null;
    private BluetoothGattCharacteristic rulerCharacteristic63 = null;
    private BluetoothGattCharacteristic rulerCharacteristic64 = null;

    private ArrayList<byte[]> fileFromDeviceChunks = new ArrayList<>();

    private final static String TAG = "DEBUG";

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothDevice device;
    public int mConnectionState = STATE_DISCONNECTED;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    private BluetoothLeService thisService = this;

    private int tryingCount = 0;

    public final static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_TRYING = "com.example.bluetooth.le.ACTION_GATT_TRYING";
    public final static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";

    public final static String ERROR_60 = "com.example.bluetooth.le.ERROR_60";
    public final static String ERROR_13 = "com.example.bluetooth.le.ERROR_13";
    public final static String ERROR_14 = "com.example.bluetooth.le.ERROR_14";
    public final static String ERROR_61 = "com.example.bluetooth.le.ERROR_61";
    public final static String ERROR_62 = "com.example.bluetooth.le.ERROR_62";
    public final static String ERROR_63 = "com.example.bluetooth.le.ERROR_63";
    public final static String ERROR_64 = "com.example.bluetooth.le.ERROR_64";
    public final static String ERROR_65 = "com.example.bluetooth.le.ERROR_65";
    public final static String DONE = "com.example.bluetooth.le.DONE";
    private String deviceAddress;

    private int fileLength;
    private long fileCrc;

    private byte[] fileResult;
    private int fileFromDeviceSize = 0;

    private byte[] fileBytesToDevice = null;
    private int fileBytesToDeviceSize = 0;

    private int writeCount = 0;

    public boolean isSendStarted = false;

    private int chunkCount = 0;
    private int totalChunkCount = 0;

    private boolean writeFileFlag = false;
    public long chunkTime;

    private ArrayList<ArrayList<Byte>> chunks = new ArrayList<>();

    private DeviceDataParser engineStat = new EngineData();
    private int statEngineCountLimit = FilesWorker.fileRowsCounter;
    private int curEngineSpeed = 0;

//    private WatchThread wt = new WatchThread(this);


    public BluetoothLeService() {
    }

    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    private static final Class<?>[] mSetForegroundSignature = new Class[]{boolean.class};
    private static final Class<?>[] mStartForegroundSignature = new Class[]{int.class, Notification.class};
    private static final Class<?>[] mStopForegroundSignature = new Class[]{boolean.class};

    private NotificationManager mNM;
    private Method mSetForeground;
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];

    void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            // Should not happen.
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e) {
            // Should not happen.
            Log.w("ApiDemos", "Unable to invoke method", e);
        }
    }

    /**
     * This is a wrapper around the new startForeground method, using the older
     * APIs if it is not available.
     */
    void startForegroundCompat(int id, Notification notification) {
        // If we have the new startForeground API, then use it.
        if (mStartForeground != null) {
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            invokeMethod(mStartForeground, mStartForegroundArgs);
            return;
        }

        // Fall back on the old API.
        mSetForegroundArgs[0] = Boolean.TRUE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
        mNM.notify(id, notification);
    }

    /**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available.
     */
    void stopForegroundCompat(int id) {
        // If we have the new stopForeground API, then use it.
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            invokeMethod(mStopForeground, mStopForegroundArgs);
            return;
        }

        // Fall back on the old API.  Note to cancel BEFORE changing the
        // foreground state, since we could be killed at that point.
        mNM.cancel(id);
        mSetForegroundArgs[0] = Boolean.FALSE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
    }

    @Override
    public void onCreate() {
        App.bus.register(this);
//        this.initialize();
//        this.connect(App.deviceAddress);
//        this.connect("C4:BE:84:71:33:87");
//        this.connect("68:C9:0B:17:E3:81");
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        try {
            mStartForeground = getClass().getMethod("startForeground",
                    mStartForegroundSignature);
            mStopForeground = getClass().getMethod("stopForeground",
                    mStopForegroundSignature);
            return;
        } catch (NoSuchMethodException e) {
            // Running on an older platform.
            mStartForeground = mStopForeground = null;
        }
        try {
            mSetForeground = getClass().getMethod("setForeground",
                    mSetForegroundSignature);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(
                    "OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String noteString = "Connected!";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.stat_notify_chat)
                        .setContentTitle("BLE connection")
                        .setContentText(noteString);
        Notification note = mBuilder.build();

        startForegroundCompat(667, note);

        deviceAddress = intent.getStringExtra("address");

        connectToDevice();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.bus.unregister(this);
        stopForegroundCompat(1);
        Log.d(TAG, String.valueOf("service destroy"));
    }

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    tryingCount = 0;
                    App.bus.post(String.valueOf(tryingCount));
                    intentAction = ACTION_GATT_CONNECTED;
                    mConnectionState = STATE_CONNECTED;
                    broadcastUpdate(intentAction);
                    Log.d(TAG, "CONNECTED to GATT server.");
                    // Attempts to discover services after successful connection.
                    mBluetoothGatt = gatt;
                    Log.d(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    close();
                    connectToDevice();
                    App.bus.post(new int[]{0, 0});

                    intentAction = ACTION_GATT_DISCONNECTED;
                    mConnectionState = STATE_DISCONNECTED;
                    Log.d(TAG, "DISCONNECTED from GATT server.");
                    broadcastUpdate(intentAction);
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
//                Toast.makeText(App.getContext(), "SERVICES", Toast.LENGTH_SHORT).show();
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                setCharacteristics();
//                fileRead();
//                fileWriteInit();
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            Log.d("BluetoothLeService", String.valueOf("write to characteristic!"));

            if (characteristic.getUuid() == rulerCharacteristic64.getUuid()) {
                App.bus.post(new CharacteristicStatus().setStatus(status));
            }

//            Log.d(TAG, "chunk " + chunkCount + " is writed");
//            Log.d("DEBUG", characteristic.getUuid().toString());
            /*Log.d(TAG, "status = " + status);
            writeCount++;*/
//Log.d("DEBUG", String.valueOf(chunkCount));
            if (characteristic.equals(characteristic3)) {
                if (writeFileFlag) {
                    if (chunkCount < chunks.size()) {
//                        WatchThread hz = wt;
                        sendChunk(chunks.get(chunkCount));  //включить при записи файла
                    }
                }
            }

            if (curEngineSpeed <= statEngineCountLimit) {
                if (characteristic.getUuid() == rulerCharacteristic62.getUuid()) {
                    readCharacteristic(rulerCharacteristic61);
                }
            }
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            Log.d("BluetoothLeService", "onReliableWriteCompleted!!!");
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
            if (characteristic == rulerCharacteristic61) {
                engineStat.handleData(null, 0, characteristic.getValue());
                try {
                    Thread.sleep(1000);
                    curEngineSpeed++;
                } catch (InterruptedException e) {
                    Log.d("BluetoothLeService", e.toString());
                }
                if (curEngineSpeed <= statEngineCountLimit) {
                    Log.d("BluetoothLeService", "curEngineSpeed = " + String.valueOf(curEngineSpeed));
                    writeEngineSpeed((byte) curEngineSpeed);
                } else {
                    stopEngine();
                }
            }
            Log.d("BluetoothLeService", Utils.byteArrayToHex(characteristic.getValue()));
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            String charactUuid = characteristic.getUuid().toString();
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);

            Log.d(TAG, String.valueOf("onCharacteristicChanged!!!"));

//            Log.d("DEBUG", "charactUuid = " + String.valueOf(charactUuid));

            if (charactUuid.equals(characteristic8UUID)) {
                byte[] result = characteristic.getValue();

//                Log.d("DEBUG", byteArrayToHex(result));

                /*byte[] ad7156Value = new byte[4];
                ad7156Value[0] = 0x0;
                ad7156Value[1] = result[6];
                ad7156Value[2] = result[5];
                ad7156Value[3] = result[4];

                ByteBuffer ad7156ValueWrapper = ByteBuffer.wrap(ad7156Value);
                int value = ad7156ValueWrapper.getInt();
                Ad7156Result adValue = new Ad7156Result(value);
                App.bus.post(adValue);

                Log.d("DEBUG", String.valueOf(value));*/

                fileFromDeviceSize += (result.length - 2);  //это была сборка кусков файла в массив после чего он парсился в файл
                fileFromDeviceChunks.add(result);
            }

            if (charactUuid.equals(characteristic7UUID)) {
                byte[] result7 = characteristic.getValue();
                byte[] resultLength = {result7[7], result7[6], result7[5], result7[4]};
                byte[] resultCRC = {0, 0, 0, 0, result7[11], result7[10], result7[9], result7[8]};
                fileLength = Utils.byteArrayToInt(resultLength);
                fileCrc = Utils.byteArrayToLong(resultCRC, 0);
                //CRC HERE AND LENGTH
            }

            if (charactUuid.equals(characteristic2UUID)) {
                byte[] result2 = characteristic.getValue();

                Log.d(TAG, "notif2 = " + Utils.byteArrayToHex(result2));

                switch (result2[0]) {
                    case 1:
                        sendFileToDevice(fileBytesToDevice);
//                        wt = new WatchThread(thisService);
//                        wt.start();
                        break;
                    case 86:
                        buildFileFromDevice();
                        break;
                    case 80:
                        stopSend();
                        broadcastUpdate(DONE);
                        break;
                    case 19:
                        stopSend();
                        broadcastUpdate(ERROR_13);
                        break;
                    case 20:
                        stopSend();
                        broadcastUpdate(ERROR_14);
                        break;
                    case 96:
                        stopSend();
                        broadcastUpdate(ERROR_60);
                        break;
                    case 97:
                        stopSend();
                        broadcastUpdate(ERROR_61);
                        break;
                    case 98:
                        stopSend();
                        broadcastUpdate(ERROR_62);
                        break;
                    case 99:
                        stopSend();
                        broadcastUpdate(ERROR_63);
                        break;
                    case 100:
                        stopSend();
                        broadcastUpdate(ERROR_64);
                        break;
                    case 101:
                        stopSend();
                        broadcastUpdate(ERROR_65);
                        break;
                }
                //начать проверку файла на целостность
            }
        }
    };

    public boolean buildFileFromDevice() {
        fileResult = new byte[fileFromDeviceSize];
        int j = 0;
        for (byte[] chunk : fileFromDeviceChunks) {
            for (int i = 2; i < chunk.length; i++) {
                fileResult[j] = chunk[i];
                j++;
            }
        }
        if (fileResult.length == 0) {
            return false;
        }
        long calculatedCrc32 = Utils.calcCrc32(fileResult, fileResult.length);
        calculatedCrc32 = Utils.crc32End(calculatedCrc32, fileResult.length);

        /*Log.d(TAG, "file length = " + fileLength);
        Log.d(TAG, "file CRC = " + fileCrc);
        Log.d(TAG, String.valueOf("----------------------------"));
        Log.d(TAG, "builded file length = " + String.valueOf(fileResult.length));
        Log.d(TAG, "result array = " + byteArrayToHex(fileResult));
        Log.d(TAG, "calculated CRC = " + String.valueOf(calculatedCrc32));*/

        if (fileLength != fileResult.length) {
            Log.d(TAG, String.valueOf("wrong file length!"));
            return false;
        }

        if (fileCrc != calculatedCrc32) {
            Log.d(TAG, String.valueOf("wrong file CRC32!"));
            return false;
        }

        Log.d(TAG, String.valueOf("All is ok. Prepare of file building!"));

        buildFile(fileResult);
        return true;
    }

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        // For all other profiles, writes the data formatted in HEX.
        final byte[] data = characteristic.getValue();

        if (data != null && data.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(data.length);
            for (byte byteChar : data)
                stringBuilder.append(String.format("%02X ", byteChar));
            intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
        }

        sendBroadcast(intent);
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        /*if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.disable();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Log.d("DEBUG", e.toString());
                }
                mBluetoothAdapter.enable();
            }
        }*/
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public void startAdvertisingScan() {
        if (mBluetoothAdapter != null) {
            this.disconnect();
            mBluetoothAdapter.startLeScan(ScanCallback);    //advertising listening enable
        }
    }

    public void stopAdvertisingScan() {
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(ScanCallback);    //advertising listening disable
        }
    }

    private DeviceDataParser parser = new LightSensorData();

    //BLE ADVERTISING
    private final BluetoothAdapter.LeScanCallback ScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            String mac = device.getAddress();
            if (mac.equals(deviceAddress)) {

            }
        }
    };

    public void resetWriteParams() {
        if (parser instanceof LightSensorData) {
            ((LightSensorData) (parser)).resetWriteParams();
        }
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        /*if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }*/

        device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");

        final Intent intentTr = new Intent(ACTION_GATT_TRYING);
        sendBroadcast(intentTr);

        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
        close();
        mBluetoothGatt = null;
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic characteristic, byte[] bytes) {
        if (mConnectionState == STATE_DISCONNECTED) {
            Log.d("DEBUG", String.valueOf("cant write! device disconnected!"));
            return false;
        }
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.d(TAG, "writeCharacteristic() : BluetoothAdapter not initialized");
            return false;
        }

        boolean result;

        if (characteristic != null) {
            int properties = characteristic.getProperties();

            if (((properties & BluetoothGattCharacteristic.PROPERTY_WRITE) == BluetoothGattCharacteristic.PROPERTY_WRITE)
                    || ((properties & BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE) == BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE)
                    || ((properties & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) == BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) {

                characteristic.setValue(bytes);
                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                result = mBluetoothGatt.writeCharacteristic(characteristic);
            } else {
                Log.d("DEBUG", "CANT WRITE!");
                result = false;
            }

            Log.d("DEBUG", String.valueOf("write " + characteristic.getUuid().toString() + " true!"));
            return result;
        } else {
            Log.d("DEBUG", String.valueOf("characteristic is null!"));
            return false;
        }
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }

    public int getConnectionState() {
        return mBluetoothGatt.getConnectionState(device);
    }

    private void setCharacteristics() {
//        mBluetoothGatt.requestMtu(512);
        service = mBluetoothGatt.getService(UUID.fromString(fileServiceUUID));
        characteristic1 = service.getCharacteristic(UUID.fromString(characteristic1UUID));
        characteristic2 = service.getCharacteristic(UUID.fromString(characteristic2UUID));
        characteristic3 = service.getCharacteristic(UUID.fromString(characteristic3UUID));
        characteristic4 = service.getCharacteristic(UUID.fromString(characteristic4UUID));
        characteristic6 = service.getCharacteristic(UUID.fromString(characteristic6UUID));
        characteristic7 = service.getCharacteristic(UUID.fromString(characteristic7UUID));
        characteristic8 = service.getCharacteristic(UUID.fromString(characteristic8UUID));

        rulerService = mBluetoothGatt.getService(UUID.fromString(commandServiceUUID));
        rulerCharacteristic61 = rulerService.getCharacteristic(UUID.fromString(rulerCharacteristicc61));
        rulerCharacteristic62 = rulerService.getCharacteristic(UUID.fromString(rulerCharacteristicc62));
        rulerCharacteristic63 = rulerService.getCharacteristic(UUID.fromString(rulerCharacteristicc63));
        rulerCharacteristic64 = rulerService.getCharacteristic(UUID.fromString(rulerCharacteristicc64));
        Log.d("DEBUG", String.valueOf("set of characteristic is done!"));
    }

    /**
     * начинает побайтовое чтение с устройства после чего необходимо будет запустить функцию построения файла
     */
    public void fileRead() {
        byte[] bytes = {0x02, 0x02, 0, 0, 0x20, 0, 0, 0, 0, 0, 0, 0};
//        byte[] bytes = {0x00, 0x00, 0x00, 0x01};
//        byte[] bytes = {0, 0, 0x02, 0}; //128kb
//        byte[] bytes = {0, 0, 0x01, 0}; //64kb
//        byte[] bytes = {0, (byte) 0x84, 0, 0}; //33kb
//        byte[] bytes = {0x21, 0, 0, 0}; //33b
//        byte[] bytes = {(byte) 0xc8, 0, 0, 0}; //200b
//        byte[] bytes = {(byte) 0x64, 0, 0, 0}; //100b
//        byte[] bytes = {(byte) 0x96, 0, 0, 0}; //150b
//        byte[] bytes = {(byte) 0xaf, 0, 0, 0}; //175b
//        byte[] bytes = {(byte) 0xb1, 0, 0, 0}; //175b
//        byte[] bytes = {0, 0x04, 0, 0}; //1kb
//        byte[] bytes = {0x11, 0, 0, 0};
        writeCharacteristic(characteristic6, bytes);
        setCharacteristicNotification(characteristic7);
        setCharacteristicNotification(characteristic8);
        setCharacteristicNotification(characteristic2);
    }

    private void setCharacteristicNotification(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, true);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    /**
     * Запись файла на device
     */
    public void fileWriteInit() {
        App.bus.post(new int[]{0, 0});
        writeFileFlag = true;
        isSendStarted = true;
        App.bus.post(Actions.SEND);

        chunkCount = 0;
        totalChunkCount = 0;
//        int id = 0x100;
//        byte[] id = {0x00, 0x00, 0x00, 0x00}; //просто тест связи
        byte[] id = {0x00, 0x00, 0x4e, (byte) 0x8b}; //отправка образа прошивки
        long crc32 = 0;
        InputStream is = null;
        try {
//            is = getAssets().open("fimg.bin");
            is = getAssets().open("fimg.bin");
            fileBytesToDevice = new byte[is.available()];
            fileBytesToDeviceSize = is.available();
            totalChunkCount = fileBytesToDeviceSize / 16;
            is.read(fileBytesToDevice);
            is.close();
            crc32 = Utils.calcCrc32(fileBytesToDevice, fileBytesToDevice.length);
            crc32 = Utils.crc32End(crc32, fileBytesToDevice.length);
        } catch (IOException e) {
            Log.d(TAG, e.toString());
        }

//        Log.d(TAG, byteArrayToHex(fileBytesToDevice));
//        Log.d(TAG, "length = " + String.valueOf(length));
//        Log.d(TAG, "CRC32 = " + crc32);

//        byte[] bytesId = reverseArray(ByteBuffer.allocate(4).putInt(id).array());
        byte[] bytesId = Utils.reverseArray(ByteBuffer.allocate(4).put(id).array());
        byte[] bytesLength = Utils.reverseArray(ByteBuffer.allocate(4).putInt(fileBytesToDeviceSize).array());
        byte[] bytesCrc = Utils.reverseArray(ByteBuffer.allocate(4).putInt((int) crc32).array());
        byte[] result = ByteBuffer.allocate(12).put(bytesId).put(bytesLength).put(bytesCrc).array();

//        Log.d(TAG, "info str = " + byteArrayToHex(result));

        if (writeCharacteristic(characteristic1, result)) {
            setCharacteristicNotification(characteristic2);
        }
    }

    private void sendFileToDevice(byte[] bytes) {
        int chunkSize = 18;
        int sendCount = 0;

        short k = 0;
        if (bytes.length > 0) {
            int i = 2;
            ArrayList<Byte> chunk = new ArrayList<>();

            for (int j = 0; j < bytes.length; j++) {
                chunk.add(bytes[j]);
                i++;
                if (i == chunkSize || j == (bytes.length - 1)) {
                    byte[] count = Utils.reverseArray(ByteBuffer.allocate(2).putShort(k).array());
                    chunk.add(0, count[0]);
                    chunk.add(1, count[1]);

                    chunks.add(chunk);
                    i = 2;
                    k++;
                    chunk = new ArrayList<>();
                }
            }
        }

        sendChunk(chunks.get(0));

        int[] result1 = {fileBytesToDeviceSize / 16, 1};
        App.bus.post(result1);




        /*int[] result1 = {bytes.length / 16, k + 1};
        App.bus.post(result1);
        sendCount++;*/

    }

    private void sendChunk(ArrayList<Byte> bytes) {
        synchronized (bleSync) {
//        Log.d("DEBUG", String.valueOf(prevTime));
            chunkTime = System.currentTimeMillis();
            if (!writeFileFlag) {
                return;
            }
            byte[] result = toPrimitives(bytes);

//        Log.d("DEBUG", "chunkCount = " + chunkCount + " chunk = " + byteArrayToHex(result));

        /*if (chunkCount == 3) {
            result[5] = (byte) 0xaf;
        }*/ //тест на искажение байтов

            boolean chunkWrite;
            chunkWrite = writeCharacteristic(characteristic3, result);
//        Log.d("DEBUG", String.valueOf("write to char3"));
            if (!chunkWrite) {
                Log.d(TAG, "cant write to characteristic3 in sendchunk Function");
            }
            chunkCount++;
            int[] result1 = {fileBytesToDeviceSize / 16, chunkCount};
            App.bus.post(result1);
        }
    }

    private byte[] toPrimitives(ArrayList<Byte> oBytes) {
        byte[] bytes = new byte[oBytes.size()];
        for (int i = 0; i < oBytes.size(); i++) {
            bytes[i] = oBytes.get(i);
        }
        return bytes;
    }

    private void buildFile(byte[] fileBytes) {
        Log.d(TAG, String.valueOf("start of file building!"));

        File root = Environment.getExternalStorageDirectory();
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            Date date = new Date();
            String curTime = dateFormat.format(date);
            FileOutputStream fos = new FileOutputStream(new File(root, "Download/fromDevice_" + curTime + ".txt"));
            fos.write(fileBytes);
            fos.close();
            Log.d(TAG, String.valueOf("record of file is over"));
        } catch (IOException e) {
            Log.d(TAG, e.toString());
        }
    }

    public void connectToDevice() {
        this.initialize();
        this.connect(deviceAddress);
        tryingCount++;
        isSendStarted = false;
        App.bus.post(String.valueOf(tryingCount));
        App.bus.post(Actions.RECONNECT);
    }

    public void stopSend() {
        synchronized (bleSync) {
            writeFileFlag = false;
            chunkCount = 0;
            totalChunkCount = 0;
//            if (wt != null) {
//                wt.stopThread();
//                wt = null;
//            }
        }
    }

    private void rulerRead() {

    }

    public void writeTime(int value) {
        /*int unixtime = (int) (System.currentTimeMillis() / 1000L);
        byte[] unixtimeBytes = reverseArray(ByteBuffer.allocate(4).putInt(unixtime).array());
        writeCharacteristic(rulerCharacteristic64, unixtimeBytes);*/

        byte[] unixtimeBytes = Utils.reverseArray(ByteBuffer.allocate(4).putInt(value).array());
        writeCharacteristic(rulerCharacteristic64, unixtimeBytes);
    }

    /*public void writeEngineSpeed(int value) {
        byte[] unixtimeBytes = Tools.reverseArray(ByteBuffer.allocate(4).putInt(value).array());
        writeCharacteristic(rulerCharacteristic62, unixtimeBytes);
    }*/

    public void readStatData() {
        readCharacteristic(rulerCharacteristic61);
    }

    public void writeEngineSpeed(byte speed) {
        writeCharacteristic(rulerCharacteristic62, new byte[]{(byte) 0x05, speed});
    }

    public void stopEngine() {
        writeCharacteristic(rulerCharacteristic62, new byte[]{(byte) 0x06, 0x00});
    }

    public void startEngineStat() {
        writeEngineSpeed((byte) curEngineSpeed);
    }

    public void downloadAndUpload() {
        Log.d("BluetoothLeService", " = " + String.valueOf(1123123));
    }
}