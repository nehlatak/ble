package com.example.android.bluetoothlegatt.model.result_handlers;

/**
 * Created by dracula on 31.10.2016.
 */

public interface ResultDataHandler {
    void handleResult(byte[] scanRecord);
}
