package com.example.android.bluetoothlegatt.model;

import java.nio.ByteBuffer;

public class RulerResult {
    public short x;
    public short y;
    public short z;
    public int time;
    public short battery;
    public byte state;



    public byte f;
    public byte m;


    public int rssi;

    public double temp;

    public String deviceAddress;
}
