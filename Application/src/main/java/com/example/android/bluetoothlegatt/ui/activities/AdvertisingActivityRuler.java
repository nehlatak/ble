package com.example.android.bluetoothlegatt.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.R;
import com.example.android.bluetoothlegatt.model.RulerResult;
import com.example.android.bluetoothlegatt.ui.base.BaseActivity;
import com.example.android.bluetoothlegatt.ui.view.ChartView;
import com.squareup.otto.Subscribe;

import butterknife.Bind;
import butterknife.OnClick;

public class AdvertisingActivityRuler extends BaseActivity {
    @Bind(R.id.x)
    TextView xValue;

    @Bind(R.id.y)
    TextView yValue;

    @Bind(R.id.z)
    TextView zValue;

    @Bind(R.id.fValue)
    TextView fValue;

    @Bind(R.id.battery)
    TextView battery;

    @Bind(R.id.mValue)
    TextView mValue;

    @Bind(R.id.rssiValue)
    TextView rssiValue;

    @Bind(R.id.addressValue)
    TextView addressValue;

    @Bind(R.id.temp)
    TextView tempValue;

    @Bind(R.id.graph1)
    ChartView graph1;

    @Bind(R.id.graph2)
    ChartView graph2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertising_activity_ruler);
    }

    @Subscribe
    public void progress(final RulerResult result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                xValue.setText(String.valueOf(result.x));
                yValue.setText(String.valueOf(result.y));
                zValue.setText(String.valueOf(result.z));

                fValue.setText(String.valueOf(result.f));
                mValue.setText(String.valueOf(result.m));

                battery.setText(String.valueOf(result.battery));

                rssiValue.setText(String.valueOf(result.rssi));
                addressValue.setText(result.deviceAddress);

                tempValue.setText(String.valueOf(result.temp));

                graph1.drawPoint(result.x, result.y);
                graph2.drawPoint(0, result.z);
            }
        });
    }

    @OnClick(R.id.clear)
    public void clear(View view) {
        graph1.clear();
        graph2.clear();
    }
}
