package com.example.android.bluetoothlegatt.model;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import com.example.android.bluetoothlegatt.model.Point;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class LineGraph {

    private TimeSeries data1 = new TimeSeries("graph1");
    private TimeSeries data2 = new TimeSeries("graph2");
    private XYMultipleSeriesDataset multiData = new XYMultipleSeriesDataset();
    private XYSeriesRenderer renderer1 = new XYSeriesRenderer();
    private XYSeriesRenderer renderer2 = new XYSeriesRenderer();
    private XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
    private GraphicalView view;
    private int count1 = 0;
    private int count2 = 0;

    public LineGraph() {
        // add data1
        multiData.addSeries(data1);
        multiData.addSeries(data2);

        // customize
        renderer1.setColor(Color.RED);
        renderer1.setPointStyle(PointStyle.CIRCLE);
        renderer1.setFillPoints(true);

        // customize
        renderer2.setColor(Color.GREEN);
        renderer2.setPointStyle(PointStyle.CIRCLE);
        renderer2.setFillPoints(true);

        // Enable zoom
        multiRenderer.setZoomButtonsVisible(true);
        multiRenderer.setXTitle("Day #");
        multiRenderer.setYTitle("Temperature");
        multiRenderer.setXLabelsColor(Color.RED);
        multiRenderer.setYLabelsColor(0, Color.RED);
        multiRenderer.setTextTypeface(Typeface.DEFAULT_BOLD);
        multiRenderer.setAxisTitleTextSize(16);

        // add single renderer1 to multiple
        multiRenderer.addSeriesRenderer(renderer1);
        multiRenderer.addSeriesRenderer(renderer2);
    }

    public GraphicalView getView(Context context) {
        view = ChartFactory.getLineChartView(context, multiData, multiRenderer);
        return view;
    }

    public void addNewPoints1(Point p) {
        if (count1 > 200) {
            data1.remove(0);
        }
        count1++;
        data1.add(p.x, p.y);
    }

    public void addNewPoints2(Point p) {
        if (count2 > 200) {
            data2.remove(0);
        }
        count2++;
        data2.add(p.x, p.y);
    }
}