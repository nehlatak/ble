package com.example.android.bluetoothlegatt.ui.activities;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.Actions;
import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.CharacteristicStatus;
import com.example.android.bluetoothlegatt.model.SensorsData;
import com.example.android.bluetoothlegatt.model.device.BleConnector;
import com.example.android.bluetoothlegatt.model.device.BleEvent;
import com.example.android.bluetoothlegatt.model.device.Device;
import com.example.android.bluetoothlegatt.model.device.FilesWorker;
import com.example.android.bluetoothlegatt.services.BluetoothLeService;
import com.example.android.bluetoothlegatt.R;
import com.example.android.bluetoothlegatt.services.DownloadService;
import com.example.android.bluetoothlegatt.ui.base.BaseActivity;
import com.example.android.bluetoothlegatt.util.AppEvents;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.squareup.otto.Subscribe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends BaseActivity {
    @Bind(R.id.put_to_sleep)
    Button sleepButton;

    @Bind(R.id.read_char)
    Button readCharButton;

    @Bind(R.id.wake_up_device)
    Button wakeUpButton;

    @Bind(R.id.fileRead)
    Button readFile;

    @Bind(R.id.startAdvertising)
    Button startAdvertisingButton;

    @Bind(R.id.button_upl)
    Button uplButton;

    @Bind(R.id.button_upl2)
    Button upl2Button;

    @Bind(R.id.writeTime)
    Button writeTime;

    @Bind(R.id.chart1)
    LineChart lineChart;

    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private boolean tryingToConnect = false;

    private TextView mConnectionState;
    private TextView mDataField;
    private TextView tryingCount;
    private Button sendFile;
    private Button stop;
    private Button connect;
    private Button disconnect;

    private ProgressBar progress;
    private String mDeviceName;
    private String deviceAddress;
    private ExpandableListView mGattServicesList;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private int section = 0;

    private ArrayList<View> viewItems = new ArrayList<>();

    private ArrayList<String> listItems = new ArrayList<String>();
    private ArrayAdapter<String> adapter;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private ListView listView;

//    private BluetoothLeService mBluetoothLeService;

    @Bind(R.id.time_value)
    EditText timeValue;

    private BleConnector deviceConnector;
    private Device device;
    private FilesWorker filesWorker;

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    /*private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
            } else if (BluetoothLeService.ACTION_GATT_TRYING.equals(action)) {
                updateConnectionState(R.string.connect_trying);
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                clearUI();
                blockControls();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                if (mBluetoothLeService != null) {
                    displayGattServices(mBluetoothLeService.getSupportedGattServices());
                    unblockControls();
                }
                //нужно принять и вывести сервисы
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }

            switch (action) {
                case BluetoothLeService.DONE:
                    updateConnectionState(R.string.done);
                    break;
                case BluetoothLeService.ERROR_13:
                    updateConnectionState(R.string.error_13);
                    break;
                case BluetoothLeService.ERROR_14:
                    updateConnectionState(R.string.error_14);
                    break;
                case BluetoothLeService.ERROR_60:
                    updateConnectionState(R.string.error_60);
                    break;
                case BluetoothLeService.ERROR_61:
                    updateConnectionState(R.string.error_61);
                    break;
                case BluetoothLeService.ERROR_62:
                    updateConnectionState(R.string.error_62);
                    break;
                case BluetoothLeService.ERROR_63:
                    updateConnectionState(R.string.error_63);
                    break;
                case BluetoothLeService.ERROR_64:
                    updateConnectionState(R.string.error_64);
                    break;
                case BluetoothLeService.ERROR_65:
                    updateConnectionState(R.string.error_65);
                    break;
            }
        }
    };*/

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        if (tryingToConnect) {
            menu.findItem(R.id.menu_refresh).setActionView(R.layout.actionbar_indeterminate_progress);
        } else {
            menu.findItem(R.id.menu_refresh).setActionView(null);
        }
        return true;
    }*/

    /*private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_control);

        getSupportActionBar().setTitle(mDeviceName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        deviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);


//        deviceAddress = "68:C9:0B:17:E3:81";
//        deviceAddress = "C4:BE:84:71:33:87";
//        deviceAddress = App.deviceAddress;


        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(deviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
//        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);
        tryingCount = (TextView) findViewById(R.id.trying_count);

        progress = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.log);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        listView.setAdapter(adapter);

        /*sendFile = (Button) findViewById(R.id.send_file);
        sendFile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mBluetoothLeService.fileWriteInit();
            }
        });

        stop = (Button) findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mBluetoothLeService.stopSend();
            }
        });*/

//        startAdvertising = (Button) findViewById(R.id.startAdvertising);
        startAdvertisingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                writeTimeValue();
//                mBluetoothLeService.disconnect();
                deviceConnector.disconnect();
                deviceConnector.startAdvertisingScan();
                Intent advIntent = new Intent(App.getContext(), AdvertisingActivityRuler.class);
                startActivity(advIntent);
            }
        });

        /*writeTime = (Button) findViewById(R.id.writeTime);
        writeTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeTimeValue();
            }
        });*/

        /*readFile = (Button) findViewById(R.id.fileRead);
        readFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBluetoothLeService.fileRead();
            }
        });

        connect = (Button) findViewById(R.id.connect);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBluetoothLeService.connectToDevice();
            }
        });*/

        disconnect = (Button) findViewById(R.id.disconnect);
        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mBluetoothLeService.disconnect();
                deviceConnector.disconnect();
            }
        });

        /*viewItems.add(sleepButton);
        viewItems.add(wakeUpButton);
        viewItems.add(uplButton);
        viewItems.add(upl2Button);*/

        viewItems.add(readCharButton);
        viewItems.add(timeValue);
        viewItems.add(writeTime);
        viewItems.add(readFile);
        viewItems.add(startAdvertisingButton);

        initGraph();
    }

    private void writeTimeValue() {
        try {
            int value = Integer.parseInt(timeValue.getText().toString());
            Timber.d("Write time value = " + String.valueOf(value));
            device.writeTime(value);
        } catch (Exception e) {
            Timber.d(" = " + String.valueOf("Error parse int value!"));
        }
    }

    private int calcProgress(int total, int value) {
        int result = 0;

        result = (value * 100) / total;

        return (int) result;
    }

    @Override
    protected void onResume() {
        super.onResume();

        blockControls();

        deviceConnector = new BleConnector(App.getContext());
        device = new Device(deviceConnector);
        filesWorker = new FilesWorker(deviceConnector, device);

        deviceConnector.connectToDevice(deviceAddress);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        deviceConnector.disconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        deviceConnector.disconnect();
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
                String strToLog = getString(resourceId);
                updateLog(strToLog);
            }
        });
    }

    private void displayData(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (data != null) {
                    mDataField.setText(data);
                }
            }
        });
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String serviceUuid = null;
        String characteristicUuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            serviceUuid = gattService.getUuid().toString();

            currentServiceData.put(LIST_NAME, unknownServiceString);
            currentServiceData.put(LIST_UUID, serviceUuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                characteristicUuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(LIST_NAME, unknownCharaString);
                currentCharaData.put(LIST_UUID, characteristicUuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2},
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    @Subscribe
    public void progress(int[] result) {
        int total = result[0];
        int curSection = result[1];
        if (total == 0 && curSection == 0) {
            progress.setProgress(0);
        } else {
            progress.setProgress(calcProgress(total, curSection));
        }
    }

    @Subscribe
    public void update(CharacteristicStatus charStatus) {
        Log.d("DeviceControlActivity", "STATUS FROM update METHOD= " + String.valueOf(charStatus.getStatus()));
    }

    @Subscribe
    public void progress(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tryingCount.setText(result);
            }
        });
    }

    @Subscribe
    public void progress(final Actions action) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (action) {
                    case RECONNECT:
                    case SEND:
                        listItems.clear();
                        adapter.notifyDataSetChanged();
                        progress.setProgress(0);
                        break;
                }
            }
        });
    }

    @Subscribe
    public void handleUIEvents(AppEvents status) {
        switch (status) {
            case DEVICE_STATE_CONNECTED:
                updateConnectionState(R.string.connected);
                break;
            case DEVICE_STATE_DISCONNECTED:
                updateConnectionState(R.string.disconnected);
                blockControls();
                break;
            case DEVICE_SERVICES_DISCOVERED:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayGattServices(deviceConnector.getSupportedGattServices());
                        unblockControls();
                    }
                });
                break;
        }
    }

    @Subscribe
    public void handleInfoEvents(BleEvent event) {
        switch (event.getEvent()) {
            case DEVICE_ON_CHARACTERISTIC_READ:

                break;
            case ACTION_DATA_AVAILABLE:
                displayData(event.getInfo());
                break;
        }
    }

    private void updateLog(String str) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        //get current date time with Date()
        Date date = new Date();
        String result = dateFormat.format(date) + " " + str;

        listItems.add(0, result);
        adapter.notifyDataSetChanged();
//        listView.setSelection(adapter.getCount() - 1);
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.smoothScrollToPosition(0);
            }
        });
    }

    @OnClick(R.id.read_char)
    public void readChar() {
        device.readStatData();
    }

    /*@OnClick(R.id.write_speed)
    public void writeSpeed() {
        mBluetoothLeService.writeEngineSpeed((byte) 0x11);
    }

    @OnClick(R.id.stop_engine)
    public void stopEngine() {
        mBluetoothLeService.stopEngine();
    }

    @OnClick(R.id.start_engine_stat)
    public void startEngineStat() {
        mBluetoothLeService.startEngineStat();
    }*/

    @OnClick(R.id.button_dl)
    public void download() {
        downloadFile(FilesWorker.repoUrl + FilesWorker.repoFileName);
        downloadFile(FilesWorker.repoUrl + FilesWorker.stackFileName);
        downloadFile(FilesWorker.repoUrl + FilesWorker.firmwareFileName);
    }

    private void downloadFile(String fileUrl) {
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("url", fileUrl);
        intent.putExtra("receiver", new DownloadReceiver(new Handler()));
        startService(intent);
    }

    @OnClick(R.id.button_upl)
    public void uploadFirmware() {
        Log.d("DeviceControlActivity", "upload firmware to device start!");
        filesWorker.uploadFirmwareToDevice(FilesWorker.firmwareFileId, FilesWorker.firmwareFileName);
    }

    @OnClick(R.id.button_upl2)
    public void uploadStack() {
        Log.d("DeviceControlActivity", "upload stack to device start!");
        filesWorker.uploadStackToDevice(FilesWorker.stackFileId, FilesWorker.stackFileName);
    }

    @OnClick(R.id.wake_up_device)
    public void wakeUp(View view) {
        Log.d("DeviceControlActivity", "wake up!");
        device.wakeUp();
    }

    @OnClick(R.id.put_to_sleep)
    public void sleep(View view) {
        Log.d("DeviceControlActivity", "sleep!");
        device.sleep();
    }

    @OnClick(R.id.writeTime)
    public void writeTime() {
        writeTimeValue();
    }

    @OnClick(R.id.fileRead)
    public void enableLight() {
        device.enableLightSensor();
    }

    @SuppressLint("ParcelCreator")
    private class DownloadReceiver extends ResultReceiver {

        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == DownloadService.UPDATE_PROGRESS) {
                int progress = resultData.getInt("progress");
                if (progress == 100) {
                    App.showMessage(resultData.getString("message"));
                }
            }
        }
    }

    private void blockControls() {
        for (View item : viewItems) {
            item.setEnabled(false);
        }
    }

    private void unblockControls() {
        for (View item : viewItems) {
            item.setEnabled(true);
        }
    }


    public void initGraph() {
        lineChart = (LineChart) findViewById(R.id.chart1);
//        lineChart.setOnChartValueSelectedListener(this);
//
        // enable description text
        lineChart.getDescription().setEnabled(true);

        // enable touch gestures
//        lineChart.setTouchEnabled(true);

        // enable scaling and dragging
//        lineChart.setDragEnabled(true);
//        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
//        lineChart.setPinchZoom(true);

        // set an alternative background color
        lineChart.setBackgroundColor(Color.LTGRAY);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        lineChart.setData(data);
    }

    private void addEntry(float y) {
        LineData data = lineChart.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(set.getEntryCount(), y), 0);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            lineChart.notifyDataSetChanged();

            // limit the number of visible entries
            lineChart.setVisibleXRangeMaximum(100);
            // lineChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            lineChart.moveViewToX(data.getEntryCount());

            // this automatically refreshes the chart (calls invalidate())
            // lineChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    @Subscribe
    public void handleSensorsData(SensorsData data) {
        String result = "sensor1 = " + String.valueOf(data.getSensor1()) + " sensor2 = " + String.valueOf(data.getSensor2()) + " sensor3 = " + String.valueOf(data.getSensor3());
        Timber.d(result);
        addEntry(data.getSensor1());
    }
}