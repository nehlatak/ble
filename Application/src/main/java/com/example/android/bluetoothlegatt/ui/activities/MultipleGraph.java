package com.example.android.bluetoothlegatt.ui.activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.example.android.bluetoothlegatt.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.Random;

public class MultipleGraph extends Activity {
    private GraphicalView mChart;
    private XYSeries graph1 = new XYSeries("Income");
    // Creating an XYSeries for Expense
    private XYSeries graph2 = new XYSeries("Expense");
    private final int dotsCount = 200;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_graph);
    }

    @Override
    protected void onResume() {
        super.onResume();
        openChart();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < dotsCount; i++) {
                    final int x = i;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            graph1.add(x, RANDOM.nextInt(4000));
                            graph2.add(x, RANDOM.nextInt(4000));
                            mChart.repaint();
                        }
                    });

                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        Log.d("DEBUG", String.valueOf("InterruptedException!!!"));
                    }
                }
            }
        }).start();
    }

    private static final Random RANDOM = new Random();

    private void openChart() {
// Adding data to Income and Expense Series
        /*for (int i = 0; i < 20; i++) {
            graph1.add(i, RANDOM.nextInt(4000));
            graph2.add(i, RANDOM.nextInt(4000));
        }*/

// Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(graph1);
        dataset.addSeries(graph2);

// Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer graph1Renderer = new XYSeriesRenderer();
        graph1Renderer.setColor(Color.CYAN);
        graph1Renderer.setPointStyle(PointStyle.CIRCLE);
        graph1Renderer.setFillPoints(true);

        XYSeriesRenderer graph2Renderer = new XYSeriesRenderer();
        graph2Renderer.setColor(Color.RED);
        graph2Renderer.setPointStyle(PointStyle.CIRCLE);
        graph2Renderer.setFillPoints(true);


// Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        /*multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle("Income vs Expense Chart");
        multiRenderer.setXTitle("Year 2014");
        multiRenderer.setYTitle("Amount in Dollars");*/

/***
 * Customizing graphs
 */
//setting text size of the title
        multiRenderer.setChartTitleTextSize(28);
//setting text size of the axis title
        multiRenderer.setAxisTitleTextSize(24);
//setting text size of the graph lable
        multiRenderer.setLabelsTextSize(24);
//setting zoom buttons visiblity
        multiRenderer.setZoomButtonsVisible(false);
//setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(false, false);
//setting click false on graph
        multiRenderer.setClickEnabled(false);
//setting zoom to false on both axis
        multiRenderer.setZoomEnabled(false, false);
//setting lines to display on y axis
        multiRenderer.setShowGridY(true);
//setting lines to display on x axis
        multiRenderer.setShowGridX(true);
//setting legend to fit the screen size
        multiRenderer.setFitLegend(true);
//setting displaying line on grid
        multiRenderer.setShowGrid(true);
//setting zoom to false
        multiRenderer.setZoomEnabled(false);
//setting external zoom functions to false
        multiRenderer.setExternalZoomEnabled(false);
//setting displaying lines on graph to be formatted(like using graphics)
        multiRenderer.setAntialiasing(true);
//setting to in scroll to false
        multiRenderer.setInScroll(false);
//setting to set legend height of the graph
        multiRenderer.setLegendHeight(30);
//setting x axis label align
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
//setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
//setting text style
        multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
//setting no of values to display in y axis
        multiRenderer.setYLabels(10);
// setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 4000.
// if you use dynamic values then get the max y value and set here
        multiRenderer.setYAxisMax(4000);
//setting used to move the graph on xaxiz to .5 to the right
//        multiRenderer.setXAxisMin(-0.5);
//setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMax(dotsCount);
//setting bar size or space between two bars
//multiRenderer.setBarSpacing(0.5);
//Setting background color of the graph to transparent
        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
//Setting margin color of the graph to transparent
        multiRenderer.setMarginsColor(getResources().getColor(R.color.transparent_background));
        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setScale(2f);
//setting x axis point size
        multiRenderer.setPointSize(4f);
//setting the margin size for the graph in the order top, left, bottom, right
        multiRenderer.setMargins(new int[]{30, 30, 30, 30});

        /*for (int i = 0; i < x.length; i++) {
            multiRenderer.addXTextLabel(i, mMonth[i]);
        }*/

// Adding incomeRenderer and expenseRenderer to multipleRenderer
// Note: The order of adding dataseries to dataset and renderers to multipleRenderer
// should be same
        multiRenderer.addSeriesRenderer(graph1Renderer);
        multiRenderer.addSeriesRenderer(graph2Renderer);

//this part is used to display graph on the xml
        LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart);
//remove any views before u paint the chart
        chartContainer.removeAllViews();
//drawing bar chart
        mChart = (GraphicalView) ChartFactory.getLineChartView(MultipleGraph.this, dataset, multiRenderer);
//adding the view to the linearlayout
        chartContainer.addView(mChart);
    }
}