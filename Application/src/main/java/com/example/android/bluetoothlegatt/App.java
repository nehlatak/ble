package com.example.android.bluetoothlegatt;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import timber.log.Timber;

public class App extends Application {
    public static Bus bus;
    private static Context context;
//    public static final String deviceAddress = "B0:B4:48:DB:0A:69";
//    public static final String deviceAddress = "68:C9:0B:16:C7:02";
//    public static final String deviceAddress = "B0:B4:48:DB:0A:31";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        bus = new Bus(ThreadEnforcer.ANY);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return super.createStackElementTag(element) + ":" + element.getLineNumber();
                }
            });
        }
    }

    public static Context getContext() {
        return context;
    }

    public static void showMessage(Object message) {
        Toast.makeText(App.getContext(), message.toString(), Toast.LENGTH_SHORT).show();
    }
}