package com.example.android.bluetoothlegatt.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.android.bluetoothlegatt.model.Point;

import java.util.ArrayList;

public class ChartView extends View {
    private final int gridStep = 200;
    private Paint paint = new Paint();
    private int maxX = 35000;
    private int maxY = 35000;
    private int minX = 35000;
    private int minY = 35000;
    private float xModificator = 0;
    private float yModificator = 0;
    private ArrayList<Point> points = new ArrayList<>();

    public ChartView(Context context) {
        super(context);
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();

//        Log.d("DEBUG", "x = " + String.valueOf(x));
//        Log.d("DEBUG", "y = " + String.valueOf(y));

        xModificator = (float) x / maxX;
        yModificator = (float) y / maxY;

//        Log.d("DEBUG", "xModificator = " + String.valueOf(xModificator));
//        Log.d("DEBUG", "yModificator = " + String.valueOf(yModificator));

        int centerX = x / 2;
        int centerY = y / 2;

        int firstLineX = centerX;
        int firstLineY = centerY;
        while (firstLineX > 0) {
            firstLineX -= gridStep;
        }
        firstLineX += gridStep;
        while (firstLineY > 0) {
            firstLineY -= gridStep;
        }
        firstLineY += gridStep;

        int radius;
        radius = 5;
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);

        canvas.drawPaint(paint);
        paint.setColor(Color.GREEN);
        canvas.drawCircle(centerX, centerY, radius, paint);

        int currentLineX = firstLineX;
        int currentLineY = firstLineY;

        paint.setColor(Color.BLACK);

        while (currentLineX < x) {
            canvas.drawLine(currentLineX, 0, currentLineX, y, paint);
            currentLineX += gridStep;
        }
        while (currentLineY < y) {
            canvas.drawLine(0, currentLineY, x, currentLineY, paint);
            currentLineY += gridStep;
        }

        paint.setColor(Color.RED);

        if (points.size() > 0) {
            for (Point point : points) {
                float resX = (float) point.x * xModificator + centerX;
                float resY = (float) point.y * yModificator + centerY;

                /*Log.d("DEBUG", "resX = " + String.valueOf(resX));
                Log.d("DEBUG", "resY = " + String.valueOf(resY));*/

                canvas.drawCircle(resX, resY, radius, paint);
            }
        }
    }

    public void drawPoint(int x, int y) {
        points.add(new Point(x, y));
        this.invalidate();
    }

    public void clear() {
        points.clear();
        this.invalidate();
    }
}