package com.example.android.bluetoothlegatt.model;

import android.bluetooth.BluetoothDevice;
import android.os.Environment;
import android.util.Log;

import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.device.FilesWorker;
import com.example.android.bluetoothlegatt.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by dracula on 28.07.2016.
 */
public class EngineData implements DeviceDataParser {
    private boolean startDumpWriteFlag = false;
    private String row = "";
    private String rowsHeader = "INADATA_POS_CH1_SHUNT_VOLT;" +
            "INADATA_POS_CH1_BUS_VOLT;" +
            "INADATA_POS_CH2_SHUNT_VOLT;" +
            "INADATA_POS_CH2_BUS_VOLT;" +
            "INADATA_POS_CH3_SHUNT_VOLT;"+
            "INADATA_POS_CH3_BUS_VOLT;"+
            "counter;"+
            "counter DEC\n";
    private String rows = rowsHeader;
    private int rowsCounter = 0;
    private int limitCounter = 999999999;
    private int rowsLimit = FilesWorker.fileRowsCounter;
    private boolean writed = false;

    /*#define INADATA_POS_CH1_SHUNT_VOLT 0
    #define INADATA_POS_CH1_BUS_VOLT  2
    #define INADATA_POS_CH2_SHUNT_VOLT 4
    #define INADATA_POS_CH2_BUS_VOLT  6
    #define INADATA_POS_CH3_SHUNT_VOLT 8
    #define INADATA_POS_CH3_BUS_VOLT  10*/

    @Override
    public void handleData(BluetoothDevice device, int rssi, byte[] scanRecord) {
        /*if (scanRecord[13] == 1) {
            startDumpWriteFlag = true;
        }
        if (startDumpWriteFlag) {*/
            if (rowsCounter <= rowsLimit) {
                Log.d("EngineData", Utils.byteArrayToHex(scanRecord));
                Log.d("EngineData", "rowsCounter = " + String.valueOf(rowsCounter));
                Log.d("EngineData", "rowsLimit = " + String.valueOf(rowsLimit));
                /*if (limitCounter != scanRecord[13]) {
                    limitCounter = scanRecord[13];*/

                    short INADATA_POS_CH1_SHUNT_VOLT = ByteBuffer.wrap(new byte[]{scanRecord[0], scanRecord[1]}).getShort();
                    short INADATA_POS_CH1_BUS_VOLT = ByteBuffer.wrap(new byte[]{scanRecord[2], scanRecord[3]}).getShort();
                    short INADATA_POS_CH2_SHUNT_VOLT = ByteBuffer.wrap(new byte[]{scanRecord[4], scanRecord[5]}).getShort();
                    short INADATA_POS_CH2_BUS_VOLT = ByteBuffer.wrap(new byte[]{scanRecord[6], scanRecord[7]}).getShort();
                    short INADATA_POS_CH3_SHUNT_VOLT = ByteBuffer.wrap(new byte[]{scanRecord[8], scanRecord[9]}).getShort();
                    short INADATA_POS_CH3_BUS_VOLT = ByteBuffer.wrap(new byte[]{scanRecord[10], scanRecord[11]}).getShort();

                    byte counter = scanRecord[12];

                    Log.d("EngineData", "INADATA_POS_CH1_SHUNT_VOLT = " + String.valueOf(INADATA_POS_CH1_SHUNT_VOLT));
                    Log.d("EngineData", "INADATA_POS_CH1_BUS_VOLT = " + String.valueOf(INADATA_POS_CH1_BUS_VOLT));
                    Log.d("EngineData", "INADATA_POS_CH2_SHUNT_VOLT = " + String.valueOf(INADATA_POS_CH2_SHUNT_VOLT));
                    Log.d("EngineData", "INADATA_POS_CH2_BUS_VOLT = " + String.valueOf(INADATA_POS_CH2_BUS_VOLT));
                    Log.d("EngineData", "INADATA_POS_CH3_SHUNT_VOLT = " + String.valueOf(INADATA_POS_CH3_SHUNT_VOLT));
                    Log.d("EngineData", "INADATA_POS_CH3_BUS_VOLT = " + String.valueOf(INADATA_POS_CH3_BUS_VOLT));
                    Log.d("EngineData", "counter = " + String.valueOf(Utils.byteArrayToHex(new byte[]{counter})));
                    Log.d("EngineData", "rowsCounter = " + String.valueOf(rowsCounter));
                    Log.d("EngineData", "-------------------------------------------------------------------");

                    row += String.valueOf(INADATA_POS_CH1_SHUNT_VOLT) + ";"
                            + String.valueOf(INADATA_POS_CH1_BUS_VOLT) + ";"
                            + String.valueOf(INADATA_POS_CH2_SHUNT_VOLT) + ";"
                            + String.valueOf(INADATA_POS_CH2_BUS_VOLT) + ";"
                            + String.valueOf(INADATA_POS_CH3_SHUNT_VOLT) + ";"
                            + String.valueOf(INADATA_POS_CH3_BUS_VOLT) + ";"
                            + String.valueOf(Utils.byteArrayToHex(new byte[]{counter})) + ";"
                            + String.valueOf((int) counter) + ";"
                            + "\n";
                    rowsCounter++;
                    rows += row;
                    row = "";

                    Log.d("EngineData", rows);

                    App.bus.post(new AdvertData().setCounter(rowsCounter));
//                }
            } else {
                if (!writed) {
                    writed = true;
                    Log.d("EngineData", String.valueOf("write to file!!!"));

                    File file;
                    FileOutputStream outputStream;
                    try {
                        file = new File(Environment.getExternalStorageDirectory(), "rows2.csv");
                        outputStream = new FileOutputStream(file);
                        outputStream.write(rows.getBytes());
                        outputStream.close();

                        resetWriteParams();

                        Log.d("EngineData", String.valueOf("outputStream.close();"));
                    } catch (IOException e) {
                        Log.d("EngineData", e.toString());
                    }
//                }
            }
        }
    }

    public void resetWriteParams() {
        writed = false;
        rowsCounter = 0;
        rows = rowsHeader;
        startDumpWriteFlag = false;
    }
}