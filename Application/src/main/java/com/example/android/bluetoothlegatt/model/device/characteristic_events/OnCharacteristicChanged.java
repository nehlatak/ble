package com.example.android.bluetoothlegatt.model.device.characteristic_events;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

public class OnCharacteristicChanged {
    public BluetoothGatt gatt;
    public BluetoothGattCharacteristic characteristic;

    public OnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        this.gatt = gatt;
        this.characteristic = characteristic;
    }

    public BluetoothGattCharacteristic getCharacteristic() {
        return characteristic;
    }

    public BluetoothGatt getGatt() {
        return gatt;
    }
}