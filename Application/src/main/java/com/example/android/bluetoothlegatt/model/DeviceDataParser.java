package com.example.android.bluetoothlegatt.model;

import android.bluetooth.BluetoothDevice;

/**
 * Created by dracula on 28.07.2016.
 */
public interface DeviceDataParser {
    void handleData(BluetoothDevice device, int rssi, byte[] scanRecord);
}
