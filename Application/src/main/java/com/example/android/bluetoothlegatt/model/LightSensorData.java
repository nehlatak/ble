package com.example.android.bluetoothlegatt.model;

import android.bluetooth.BluetoothDevice;
import android.os.Environment;
import android.util.Log;

import com.example.android.bluetoothlegatt.App;
import com.example.android.bluetoothlegatt.model.device.FilesWorker;
import com.example.android.bluetoothlegatt.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by dracula on 28.07.2016.
 */
public class LightSensorData implements DeviceDataParser {
    private boolean startDumpWriteFlag = false;
    private String row = "";
    private String rows = "lux value1;lux value2;lux value3;counter;counter DEC\n";
    private int rowsCounter = 0;
    private int limitCounter = 999999999;
    private int rowsLimit = FilesWorker.fileRowsCounter;
    private boolean writed = false;

    @Override
    public void handleData(BluetoothDevice device, int rssi, byte[] scanRecord) {
//                if (scanRecord[13] == 0) {
        if (scanRecord[13] == 1) {
            startDumpWriteFlag = true;
        }
        if (startDumpWriteFlag) {
            if (rowsCounter <= rowsLimit) {
                if (limitCounter != scanRecord[13]) {
                    limitCounter = scanRecord[13];
                    double value1 = Utils.calcLux(scanRecord[7], scanRecord[8]);
                    double value2 = Utils.calcLux(scanRecord[9], scanRecord[10]);
                    double value3 = Utils.calcLux(scanRecord[11], scanRecord[12]);

                    byte counter = scanRecord[13];

                    String sensor1 = Utils.calcSensorsValues(scanRecord[14]);
                    String sensor2 = Utils.calcSensorsValues(scanRecord[15]);
                    String sensor3 = Utils.calcSensorsValues(scanRecord[16]);

                    Log.d("BluetoothLeService", "scanRecord = " + String.valueOf(Utils.byteArrayToHex(scanRecord)));

                    Log.d("BluetoothLeService", "value1 = " + String.valueOf(value1));
                    Log.d("BluetoothLeService", "value2 = " + String.valueOf(value2));
                    Log.d("BluetoothLeService", "value3 = " + String.valueOf(value3));
                    Log.d("BluetoothLeService", "counter = " + String.valueOf(Utils.byteArrayToHex(new byte[]{counter})));
                    Log.d("BluetoothLeService", "rowsCounter = " + String.valueOf(rowsCounter));
                    Log.d("BluetoothLeService", "sensor1 = " + String.valueOf(sensor1));
                    Log.d("BluetoothLeService", "sensor2 = " + String.valueOf(sensor2));
                    Log.d("BluetoothLeService", "sensor3 = " + String.valueOf(sensor3));
                    Log.d("BluetoothLeService", "-------------------------------------------------------------------");

                    row += String.valueOf((int) value1) + ";"
                            + String.valueOf((int) value2) + ";"
                            + String.valueOf((int) value3) + ";"
                            + String.valueOf(Utils.byteArrayToHex(new byte[]{counter})) + ";"
                            + String.valueOf((int) counter) + ";"
                            + sensor1 + ";"
                            + sensor2 + ";"
                            + sensor3 + ";"
                            + "\n";
                    rowsCounter++;
                    rows += row;
                    row = "";

                    Log.d("BluetoothLeService", rows);

                    App.bus.post(new AdvertData().setCounter(rowsCounter));
                }
            } else {
                if (!writed) {
                    writed = true;
                    Log.d("BluetoothLeService", String.valueOf("write to file!!!"));

                    File file;
                    FileOutputStream outputStream;
                    try {
                        file = new File(Environment.getExternalStorageDirectory(), "rows2.csv");
                        outputStream = new FileOutputStream(file);
                        outputStream.write(rows.getBytes());
                        outputStream.close();
                        Log.d("BluetoothLeService", String.valueOf("outputStream.close();"));
                    } catch (IOException e) {
                        Log.d("BluetoothLeService", e.toString());
                    }
                }
            }
        }
    }

    public void resetWriteParams() {
        writed = false;
        rowsCounter = 0;
        rows = "lux value1;lux value2;lux value3;counter\n";
        startDumpWriteFlag = false;
    }
}
