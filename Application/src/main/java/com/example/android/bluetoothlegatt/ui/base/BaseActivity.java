package com.example.android.bluetoothlegatt.ui.base;

import android.support.v7.app.AppCompatActivity;

import com.example.android.bluetoothlegatt.App;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        App.bus.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.bus.unregister(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}