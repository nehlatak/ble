package com.example.android.bluetoothlegatt.model.device;

public final class DeviceCharacteristics {
    private DeviceCharacteristics() {
    }

    public static final String fileServiceUUID = buildFullUuid("fef0");
    public static final String commandServiceUUID = buildFullUuid("fe60");

    public static final String characteristic1UUID = buildFullUuid("fef1");
    public static final String characteristic2UUID = buildFullUuid("fef2");
    public static final String characteristic3UUID = buildFullUuid("fef3");
    public static final String characteristic4UUID = buildFullUuid("fef4");
    public static final String characteristic5UUID = buildFullUuid("fef5");
    public static final String characteristic6UUID = buildFullUuid("fef6");
    public static final String characteristic7UUID = buildFullUuid("fef7");
    public static final String characteristic8UUID = buildFullUuid("fef8");
    public static final String characteristic9UUID = buildFullUuid("fef9");
    public static final String characteristicaUUID = buildFullUuid("fefa");
    public static final String characteristicbUUID = buildFullUuid("fefb");
    public static final String characteristiccUUID = buildFullUuid("fefc");

    public static final String characteristicFE61 = buildFullUuid("fe61");
    public static final String characteristicFE62 = buildFullUuid("fe62");
    public static final String characteristicFE63 = buildFullUuid("fe63");
    public static final String characteristicFE64 = buildFullUuid("fe64");

    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    private static String buildFullUuid(String shortUuid) {
//        return "f000" + shortUuid + "-0451-4000-b000-000000000000";
        return "6e40" + shortUuid + "-b5a3-f393-e0a9-e50e24dcca9e";   //nordic
    }
}